#! /bin/bash
#DESCRIPCIO
#AUTOR: Adrian Martinez Montes
mkdir $HOME/DIR_BASE $HOME/DIR_BASE/DIR1 $HOME/DIR_BASE/DIR2 $HOME/DIR_BASE/DIR3 &> /dev/null
whoami > $HOME/DIR_BASE/DIR1/nom.txt 2> /dev/null
hostname > $HOME/DIR_BASE/DIR2/maquina.txt 2> /dev/null
whoami > $HOME/DIR_BASE/DIR3/resum.txt 2> /dev/null
hostname >> $HOME/DIR_BASE/DIR3/resum.txt 2> /dev/null
date >> $HOME/DIR_BASE/DIR3/resum.txt 2> /dev/null
chmod 775 $HOME/DIR_BASE/DIR1 &> /dev/null
chmod 775 $HOME/DIR_BASE/DIR2 &> /dev/null
chmod 775 $HOME/DIR_BASE/DIR3 &> /dev/null
chmod 600 $HOME/DIR_BASE/DIR1/nom.txt &> /dev/null
chmod 640 $HOME/DIR_BASE/DIR2/maquina.txt &> /dev/null
chmod 664 $HOME/DIR_BASE/DIR3/resum.txt &> /dev/null
