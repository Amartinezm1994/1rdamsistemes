#! /bin/bash
#DESCRIPCIÓ:
#escriure a pantalla "Esborrant"
#escriure a pantalla el directori actual
#esborrar el subdirectori AUX
#AUTOR: Adrian Martínez
echo ESBORRANT
pwd &> errors_esborrar.txt
rm -r AUX &>> errors_esborrar.txt
