#! /bin/bash
# DESCRIPCIÓ:
# 	Crea un fitxer a /tmp amb el nom d'usuari, nom maquina, anometat DADES.txt.
#	Copia aquest fitxer al home/seguretat de la maquina virtual (té que estar creada amb anterioritat).
#	Copia la carpeta /etc al home/seguretat de la maquina virtual.
# AUTOR: Adrian Martínez Montes
RUTA=/tmp/DADES.txt
IP=ausias@192.168.1.207:/home/ausias/seguretat
NOPRINT=/dev/null
touch $RUTA 2> $NOPRINT
hostname >> $RUTA 2> $NOPRINT
whoami >> $RUTA 2> $NOPRINT
scp $RUTA $IP &> $NOPRINT
scp -r /etc $IP &> $NOPRINT
echo "TRANSFERENCIA FINALITZADA"
