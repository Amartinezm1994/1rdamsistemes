#! /bin/bash
#DESCRIPCIO:
#Crea 3 directoris a la carpeta actual.
#Emmagatzema el nom d'usuari a: DIR1/nom.txt
#Emmagatzema el nom de la m�quina a: DIR2/maquina.txt
#Entra a la carpeta DIR3
#Emmagatzema nom usuari, nom m�quina i data a resum.txt
#AUTOR: Adrian Mart�nez
mkdir DIR1
mkdir DIR2
mkdir DIR3
whoam > DIR1/nom.txt
hostname > DIR2/maquina.txt
cd DIR3
whoam > resum.txt
hostname >> resum.txt
date >> resum.txt