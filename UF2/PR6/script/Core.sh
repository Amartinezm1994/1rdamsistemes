#! /bin/bash

#AUTORS:
# -Adrian Martínez
# -Núria Alcalà
# -Ismael Soriano
# -Nil Llisterri

#Arguments passats a aquest script:
# - $1  -> primer disc físic (ex. "sdb")
# - $2  -> segon disc físic (ex. "sdc")
# - $3  -> tercer disc físic (ex. "sdd")
# - $4  -> quart disc físic (ex. "sde")
# - $5  -> ruta on es faràn les proves (ex. "/media")
# - $6  -> mida del cluster (ex. "1024")
# - $7  -> carpeta tmp del clúster actual (ex. "/tmp/Prova/Cluster1024B")
# - $8  -> arxiu de variables (ex. "/tmp/Prova/variables")

#Nom arxiu & arguments:
# ./Execution.sh "FS" "disc fisic" "ruta" "cluster" "ruta temporal/FS"

#----------------------------
#---------SOFTCODE-----------
#----------------------------

#Sistemes de fitxers


FSTIME="totalFSs.txt"

#----------------------------
#---------INFOCODE-----------
#----------------------------

#---Introducció de dades en arxius d'informació
MissIntrTmpFs="-----Temps total per al Sistema de fitxers:"


#----------------------------
#---------HARDCODE-----------
#----------------------------

RUTAOP="$5"
RUTATMP="$7"

printf "\n ---------------------------------------------------"
printf "\n -------------- Mida de clúster: $6 --------------\n"
printf " ---------------------------------------------------\n"

for i in 1 2 3 4 #Un per cada sistema de fitxers
do
	declare FS$i=`cat "$8" | head -n "$i" | tail -n 1`
	#Referències amb punters: FFS=FS2 (Quan $i = 2)
	FFS=FS$i
	#Ara volem esbrinar el valor de FS2, ho fem amb un punter i amb la sentència "eval"
	#	¿Quin és el valor de FS2? (recordem que FFS=FS2)
	#	Llavors amb un punter fem un salt i cridem a la variable FS2 a través de FSS.
	#	VAR -> $FFS -> $FS2 = EXT4 ; VAR = EXT4 <- Resultat
	eval VAR=\$$FFS
	#$VAR = (ex. NTFS)
	
	#Si qualsevol de les comprovacions falla no entrarà a fer les operacions amb el mateix
	#	sistema de fitxers, regit per aquesta variable.
	COMPR=0
	
	#El sistema de fitxers "REISER" no deixa fer una formatació en menys de 4096 B de
	#	mida de clúster ni de més de 8192 B.
	if [[ "$VAR" = "REISER" ]] && ((( $6 < 4096 )) || (( $6 > 8192 )))
	then
		printf "\n	Mida de clúster ($6) invàlida per a REISER, s'omet aquest pas.\n"
		COMPR=1
	#El sistema de fitxers "EXT4" no deixa fer una formatació de menys de 1024 B i 
	#	més de 8191 (no deixa de 8192).
	elif [[ "$VAR" = "EXT4" ]] && ((( $6 < 1024 )) || (( $6 > 8191 )))
	then
		printf "\n	Mida de clúster ($6) invàlida per a EXT4, s'omet aquest pas.\n"
		COMPR=1
	#Per fer aquesta comprovació tenim que saber si el valor de clúster és
	#	múltiple de 512 que son les úniques que accepta, aquest FS accepta 
	#	una mida màxima de 65536B.
	elif [[ "$VAR" = "XFS" ]]
	then
		#65536 / 512 = 128, per tant comprovarem que el valor de clúster a
		#	analitzar sigui múltiple de 512.
		for k in {1..128}
		do
			#Si aquesta comparació és certa vol dir que la mida de clúster
			#	és múltiple de 512.
			if (( $k == $6 / 512 ))
			then
				break
			fi
		done
		#Si arriba al final i la comparació ha sigut falsa vol dir que no és
		#	múltiple de 512 llavors ometrà aquest Pas.
		if (( $k == 128 ))
		then
			printf "\n	Mida de clúster ($6) invàlida per a XFS, s'omet aquest pas.\n"
			COMPR=1
		fi
	#Per a NTFS només accepta potències de 2 a partir del resultat de 512B.
	elif [[ "$VAR" = "NTFS" ]]
	then
		if ! (( $6 == 512 || $6 == 1024 || $6 == 2048 || $6 == 4096 || $6 == 8192 || $6 == 16384 || $6 == 32768 || $6 == 65536 ))
		then
			printf "\n	Mida de clúster ($6) invàlida per a NTFS, s'omet aquest pas.\n"
			COMPR=1
		fi
	fi
	if (( "$COMPR" == 0 ))
	then
		#Per saber el nom del disc fem el mateix:
		#	DISCO -> $i -> $2 = sdc (Quan $i = 2) ; DISCO = sdc <- Resultat
		eval DISCO=\$$i
		#Amb això obtenim dos valors d'un, el valor de la $i la convertim en part d'una variable.
		#Creem carpeta amb el nom obtingut, que serà el FS.
		mkdir "$RUTATMP"/"$VAR"
		#Si no existeix el fitxer on es depositarà el temps total del FS, crea'l.
		if ! [ -e "$RUTATMP"/"$FSTIME" ]
		then
			touch "$RUTATMP"/"$FSTIME"
		fi
		printf "\n$MissIntrTmpFs $VAR\n" >> "$RUTATMP"/"$FSTIME"
		COMMAND="./Execution.sh $VAR $DISCO $RUTAOP $6 $RUTATMP/$VAR"
		#Executem Execution.sh mesurant el temps.
		/usr/bin/time -p -a -o "$RUTATMP/$FSTIME" bash -c "$COMMAND"
	fi
done
