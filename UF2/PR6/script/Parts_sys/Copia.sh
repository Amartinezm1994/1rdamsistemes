#! /bin/bash
#AUTORS:
# -Adrian Mart�nez
# -N�ria Alcal�
# -Ismael Soriano
# -Nil Llisterri
#Nota: encara que doni un error a l'arxiu de text el temps es real, hem tingut
#	que excloure aquestes 5 carpetes perqu� mai finalitzava el copiat.


#--------------------------
#--------HARDCODE----------
#--------------------------

cd /
cp -Rf `ls | grep -v media | grep -v proc | grep -v usr | grep -v var | grep -v sys` "$1" &> /dev/null
