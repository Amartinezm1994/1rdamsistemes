#! /bin/bash
#
# Formateja una partició amb un sistema de fitxers i un tamany de clúster
# definits als paràmetres.
#
#	Executar amb sudo!!
#
#AUTORS:
# -Adrian Martínez
# -Núria Alcalà
# -Ismael Soriano
# -Nil Llisterri
#
#############
# Variables #
#############

MPOINT="$1"/"$4"_FS
CLUSTER="$2"
DISC="$3"
RUTADISC=/dev/"$3"1

############
#  Script  #
############

#Segons sistema de fitxers i clúster es necessitarà d'una
#	comanda en concret per a fer la formatació dels
#	discs.

# Formateja el disc amb un sistema de fitxers
printf "\n	Formatant la partició de $DISC...\n"

#NTFS utilitza sempre la mateixa comanda per formatar.
if [[ "$4" = 'NTFS' ]]
then
	mkfs.ntfs -c "$CLUSTER" "$RUTADISC" &> /dev/null
elif [[ "$4" = 'EXT4' ]]
then
		#EXT4 et demana una confirmació si formatejas en una mesura
		#	de clúster superior a 4096.
		if (( "$CLUSTER" > 4096 ))
		then
			(echo s) | mkfs.ext4 -b "$CLUSTER" "$RUTADISC" &> /dev/null
		else
			mkfs.ext4 -b "$CLUSTER" "$RUTADISC" &> /dev/null
		fi
		#Treiem l'espai reservat per a root del disc on hàgim formatat
		#	en EXT4
		printf "\n	Esborrant percentatge reservat per a root del sistema de fitxers ext4...\n"
		tune2fs -m0 "$RUTADISC" &> /dev/null
		printf "	Percentatge reservat esborrat\n"
elif [[ "$4" = 'XFS' ]]
then
	#XFS necessita d'un paràmetre diferent si la mesura de clúster
	#	supera els 4KB, en cas de no fer-ho així no deixa muntar el disc.
	if (( "$CLUSTER" > 4096 ))
	then
		mkfs.xfs -n size="$CLUSTER" -f "$RUTADISC" &> /dev/null
	else
		mkfs.xfs -b size="$CLUSTER" -f "$RUTADISC" &> /dev/null
	fi
else
	#REISER et demana confirmació si volem formatar amb un "force".
	(echo y) | mkfs.reiserfs -b "$CLUSTER" -f "$RUTADISC" &> /dev/null
fi

printf "	$DISC formatat\n"

# Si es ext4 treu la reserva de root

# Munta la partició
printf "\n	Muntant disc $RUTADISC a $MPOINT...\n"
mount "$RUTADISC" "$MPOINT"
printf "	Disc muntat.\n"
