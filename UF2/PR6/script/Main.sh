#! /bin/bash

#AUTORS:
# -Adrian Martínez
# -Núria Alcalà
# -Ismael Soriano
# -Nil Llisterri


#Arguments passats a aquest script:
# - $1 -> ruta on es faràn les proves (ex. "/media")


#ÚTILS:
# /usr/bin/time -p -a -o "rutatxt" bash -c "comanda"
# lshw -class disk | grep /dev | cut -d ':' -f 2 | cut -d '/' -f 3
# I=PRIMER;declare "$I"DISC=1;echo "$PRIMERDISC"
#		per a veure el funcionament del declare per a un nom d'una variable formada
#		per més variables.
# fdisk -l | grep sdb | head -n 1 | cut -d ',' -f 2 | cut -d ' ' -f 2


#----------------------------
#---------SOFTCODE-----------
#----------------------------

#Indica la carpeta on es depositarà la carpeta de temps.
RUTATMP="/tmp"

#Carpeta de temps, es crea primer, aquí es depositan els temps.
DIRACREARTMP="Prova" #Important que no acabi per "/" -> NO: "Prova/"

#Arxiu dins de la carpeta anterior, es deposita el temps total de les operacions
# de clúster.
CLTIME="timeclusters.txt" #Té que ser un arxiu de text plà.

#Nom de l'arxiu on es depositaràn les variables adicionals.
TMPVARARX="variables" #Qualsevol extensió de fitxer és vàlida, simplement possar un nom correcte.

#Mida de clúster a analitzar, es pot posar qualsevol valor, no es garanteix
# que fagi les operacions pertinents per incompatibilitat de mida de clúster.
#NOTA: Només possar-hi valors númerics.
PETITCLUSTER=1024
MITJACLUSTER=4096
GRANCLUSTER=16384

#Discs físics que s'establiràn per defecte en cas de complir certs requisits.
DISCDEF1='sdb'
DISCDEF2='sdc'
DISCDEF3='sdd'
DISCDEF4='sde'

#Comprovació de tamany del disc dur
TAMANYMAXIM=8000000000 #En octets (Bytes) 8 milions de KB's, 8 GB


#----------------------------
#---------INFOCODE-----------
#----------------------------

#ATENCIÓ: SI CANVIES ALGÚN MISSATGE PROCURA QUE S'ENTENGUI O NO ÉS SABRA L'ERROR A L'HORA DE LA FALLADA!

#---Missatges d'informació
MissDiscsDef="Vols entrar en el mode comprovació de discs? En cas contrari deixar els discs per defecte com:"
MissDiscsDefWithout="El programari lshw no està instalat. Vols cancel·lar comprovacions i deixar com a discs per defecte:"
MissAssignDisc="Introdueïx un nom de disc (ex. sda) pel"

#---Missatges d'error
MissErrorOpcio="Error, introdueïx una opció correcta."
MissErrorDisc="Error, introdueïx un disc vàlid."
MissErrorDiscFound="Error, disc no trobat."
MissErrorDiscReply="Error, aquest disc ja l'has introduït."

#---Missatges d'error de finalització
MissErrorCancel="Error, carpeta especificada no trobada." #Si AUX = 1
MissErrorPermis="Error, no has executat aquest script com a sudo." #Si AUX = 2
MissErrorMissingScript="Error, no existeix el següent fitxer script:" #Si AUX = 3
MissErrorLSHW="Error, no està instal·lat el programari lshw, per a instalar-ho executa: sudo apt-get install lshw" #Si AUX = 4
MissErrorDirTmp="Error, la carpeta per ubicar fitxers de temps no existeix." #Si AUX = 5
MissErrorEspai="Error, el disc no té la suficient capacitat per a l'execució de l'script:" #Si AUX = 6
MissErrorPermisFile="Error, no hi han permisos d'execució en el següent fitxer:" #Si AUX = 7
MissErrorDiscDef="Error, disc assignat per defecte inexistent:" #Si AUX = 8
MissErrorProgsFS="Error, no hi està instalat un programa indispensable:" #Si AUX = 9

#---Introducció de dades en arxius d'informació
MissIntrTmpClust="-----Temps total per a una mida de clúster de"

#----------------------------
#---------HARDCODE-----------
#----------------------------

#Només es pot canviar l'ordre, no es pot afegir cap FS diferent i tenen que anar amb
# mayúscules, del contrari donaría error, per això hi està al hardcode.
#----No tocar FSs------
FS1='NTFS'
FS2='EXT4'
FS3='XFS'
FS4='REISER'
#----------------------

#Definim l'argument passat a aquest script com a una variable.
RUTA="$1"
AUX=0
COMPR=0

#Codi estructurat per if aux == 0 per fer la lectura i la modificació més cómode.

#------------------------------------------------------------------------------------------
#--- Comprovació si és root ---------------------------------------------------------------
#Volem comprovar si s'executa amb root, per això obtenim la ID de l'usuari 
#	que hi ha actualment al terminal. Al ser sudo obtindrem "0".
printf "\n -> Comprovació root... "
if (( `id -u` != 0 ))
then
	AUX=2
	printf " error.\n"
else
	printf " OK.\n"
fi
#------------------------------------------------------------------------------------------



#------------------------------------------------------------------------------------------
#--- Comprovació si existeix carpeta introduïda -------------------------------------------
#Comprovarem que existeix la carpeta introduïda, en cas contrari saltarà un error
#	i cancel·larà l'execució del programa.
if (( $AUX == 0 ))
then
	printf " -> Comprovació carpeta introduïda... "
	if ! [ -d "$RUTA" ]
	then
		printf " error.\n"
		AUX=1
	else
		printf " OK.\n"
	fi
fi
#------------------------------------------------------------------------------------------




#------------------------------------------------------------------------------------------
#--- Comprovació si fitxers essencials existeixen i tenen permissos d'execució ------------
#Si qualsevol subscript no existeix el programa donaria un error d'execució
#	més endavant. També tenim que revisar els permisos de cada fitxer, simplement
#	han de tindre el permís d'execució.
if (( $AUX == 0 ))
then
	printf " -> Comprovació d'existencia i permissos de fitxers:\n"
	#Per cada fitxer de l'script donarà una volta definint la variable com a nom del subscript.
	#IMPORTANT -- DEPOSITAR AQUÍ TOTS ELS FITXERS DE L'SCRIPT PER A FER LA COMPROVACIÓ!
	for i in Core.sh Execution.sh Petits.sh Parts_petits/Crea.sh Parts_petits/Borra.sh Parts_petits/Mou.sh Formateig.sh Grans.sh Parts_grans/Borra.sh Parts_grans/Copia.sh Parts_grans/Crea.sh Parts_grans/Mou.sh System.sh Parts_sys/Copia.sh Parts_sys/Mou.sh Parts_sys/Borra.sh
	#---------------------------------------------------------------------------------
	do
		printf "	$i... "
		#Comprova si existeix el fitxer.
		if ! [ -e "$i" ]
		then
			printf "error.\n"
			AUX=3
			ErrNOMARXIU="$i"
			break
		#Comprova que té permissos.
		elif ! [ -x "$i" ]
		then
			printf "error.\n"
			AUX=7
			ErrNOMARXIU="$i"
			break
		else
			printf "OK.\n"
		fi
	done
fi
#------------------------------------------------------------------------------------------



#------------------------------------------------------------------------------------------
#--- Comprovació si els programes de formatació XFS i Reiser estàn instalats --------------
#Si qualsevol programa de formateig no existeix segurament sorgiràn errors a l'hora
#	de fer els tests. Volem que això no passi.
if (( $AUX == 0 ))
then
	printf " -> Comprovació programes de formatació instal·lats.\n"
	printf "	xfsdump... "
	#Es sitúa a la "paraula" 7 amb un cut, al fer un dpkg posa la paraula "install" a
	#	la posició 7 sempre, amb això podem saber si aquest paquet està instalat. Si no
	#	està instalat mostra error i cancel·la programa.
	if ! [[ `dpkg --get-selections xfsdump | cut -f 7` = 'install' ]]
	then
		printf "error.\n"
		AUX=9
		Program="xfsdump"
	else
		printf "OK.\n"
		#Un for per cada programa a revisar on es situi la paraula "install" a la posició
		#	6, en aquest tenim 3 casos, per tant es convenient utilitzar-ho.
		for i in xfsprogs reiser4progs reiserfsprogs
		do
			printf "	$i... "
			if ! [[ `dpkg --get-selections "$i" | cut -f 6` = 'install' ]]
			then
				printf "error.\n"
				AUX=9
				Program="$i"
				break
			else
				printf "Ok.\n"
			fi
		done
	fi
fi
#------------------------------------------------------------------------------------------




#------------------------------------------------------------------------------------------
#--- Petició si entrar en mode comprovació de discs ---------------------------------------
#--- Comprovació si el programari LSHW està instalat --------------------------------------
#Amb el programari LSHW analitzarem els discs però abans volem saber si l'usuari vol
#	escollir el métode per defecte on es definiràn els discs per defecte com hàgim
#	posat a les variables, si volem el métode on escollim discs físics comprovarem
#	que el software de LSHW està instalat, del contrari, li tornarem a preguntar
#	a l'usuari si volem deixar els discs per defecte com hem definit a les variables
#	del softcore si la resposta és no es cancel·la el programa.
if (( $AUX == 0 ))
then
	while [[ $OPCIO != 's' ]] && [[ $OPCIO != 'n' ]]
	do
		#Amb un read llegim dades introduïdes per l'usuari i amb el -p mostrem el missatge.
		read -p "$MissDiscsDef $DISCDEF1, $DISCDEF2, $DISCDEF3 i $DISCDEF4? [s/n] " OPCIO
		if [[ $OPCIO != 's' ]] && [[ $OPCIO != 'n' ]]
		then
			echo "$MissErrorOpcio"
		fi
	done
	if [[ $OPCIO = 's' ]]
	then
		#Sabem que la posició del install del dkpg get selections és la 7.
		if ! [[ `dpkg --get-selections lshw | cut -f 7` = 'install' ]]
		then
			while [[ $CONF != 's' ]] && [[ $CONF != 'n' ]]
			do
				read -p "$MissDiscsDef $DISCDEF1, $DISCDEF2, $DISCDEF3 i $DISCDEF4? [s/n] " CONF
				if [[ $CONF != 's' ]] && [[ $CONF != 'n' ]]
				then
					echo "$MissErrorOpcio"
				fi
			done
			if [[ $CONF = 'n' ]]
			then
				AUX=4
			fi
		fi
	fi
	if [[ $OPCIO = 'n' ]] || [[ $CONF = 's' ]]
	then
		#Definim discs i la variable COMPR la fiquem a 1 per no passar per el mode comprovació.
		DISCPRIMER=$DISCDEF1
		DISCSEGON=$DISCDEF2
		DISCTERCER=$DISCDEF3
		DISCQUART=$DISCDEF4
		COMPR=1
	fi
fi
#------------------------------------------------------------------------------------------




#------------------------------------------------------------------------------------------
#--- Mode comprovació: demana a l'usuari els discs i comprova que siguin correctes --------
#Si del contrari hem dit que volem entrar en mode comprovació llavors és on demanarem
#	4 discs a l'usuari, en cada disc farem 3 comprovacions: que sigui vàlid, que
#	no es repeteixi i que existeixi; amb aquest métode podem saber amb certesa
#	que l'usuari introduirà un disc vàlid.
#Aquesta petita funció admet discs en qualsevol ordre.
if (( $AUX == 0 && $COMPR == 0))
then
	#Inicialitzem una altre comprovació a 1.
	CONF=1
	#Mentre CONF sigui = 1 segueix fent el blucle, en cas de que l'usuari introdueïxi
	#	un disc correcte farà un break i saltarà al següent while del següent disc.
	while (( $CONF == 1 ))
	do
		#Mentres el disc sigui "sd" seguit d'una lletra minúscula de la a a la z.
		while [[ "$DISCPRIMER" != sd[a-z] ]]
		do
			read -p "$MissAssignDisc primer disc: " DISCPRIMER
			if [[ "$DISCPRIMER" != sd[a-z] ]]
			then
				echo "$MissErrorDisc"
			fi
		done
		#Fem el lshw -class disk que es per mostrar dispositius físics d'emmagatzematge
		#	amb els resultats obtinguts buscarem el disc introduït, ens apareixerà 1 línia,
		#	que és la que relacionarem amb el disc que ha introduït l'usuari, allà només
		#	volem la paraula "sd*", llavors farem dos cuts que ens portaràn a aquesta paraula.
		#	Aquest resultat ho comparem amb el introduït pel usuari i si és certa la comparació
		#	trenquem bucle.
		if [[ $DISCPRIMER = `lshw -class disk | grep /dev/$DISCPRIMER | cut -d ':' -f 2 | cut -d '/' -f 3` ]]
		then
			break
		else
			#Missatge d'error, declarem com a 0 aquesta variable per a que entri
			#	dins el while on es demana la introducció de dades.
			DISCPRIMER=0
			echo "$MissErrorDiscFound"
		fi
	done
	#Mateix procés però amb la excepció de que aquest també compara amb l'anterior disc introduït
	#	per l'usuari.
	while (( $CONF == 1 ))
	do
		while [[ "$DISCSEGON" != sd[a-z] ]]
		do
			read -p "$MissAssignDisc segon disc: " DISCSEGON
			if [[ "$DISCSEGON" != sd[a-z] ]]
			then
				echo "$MissErrorDisc"
			fi
		done
		#Aquí compara que no sigui igual a cap disc introduït anteriorment.
		if [[ $DISCSEGON = $DISCPRIMER ]]
		then
			DISCSEGON=0
			echo "$MissErrorDiscReply"
		elif ! [[ $DISCSEGON = `lshw -class disk | grep /dev/$DISCSEGON | cut -d ':' -f 2 | cut -d '/' -f 3` ]]
		then
			DISCSEGON=0
			echo "$MissErrorDiscFound"
		else
			break
		fi
	done
	#Mateix procés.
	while (( $CONF == 1 ))
	do
		while [[ "$DISCTERCER" != sd[a-z] ]]
		do
			read -p "$MissAssignDisc tercer disc: " DISCTERCER
			if [[ "$DISCTERCER" != sd[a-z] ]]
			then
				echo "$MissErrorDisc"
			fi
		done
		if [[ $DISCTERCER = $DISCPRIMER ]] || [[ $DISCTERCER = $DISCSEGON ]]
		then
			DISCTERCER=0
			echo "$MissErrorDiscReply"
		elif ! [[ $DISCTERCER = `lshw -class disk | grep /dev/$DISCTERCER | cut -d ':' -f 2 | cut -d '/' -f 3` ]]
		then
			DISCTERCER=0
			echo "$MissErrorDiscFound"
		else
			break
		fi
	done
	while (( $CONF == 1 ))
	do
		while [[ "$DISCQUART" != sd[a-z] ]]
		do
			read -p "$MissAssignDisc quart disc: " DISCQUART
			if [[ "$DISCQUART" != sd[a-z] ]]
			then
				echo "$MissErrorDisc"
			fi
		done
		if [[ $DISCQUART = $DISCPRIMER ]] || [[ $DISCQUART = $DISCSEGON ]] || [[ $DISCQUART = $DISCTERCER ]]
		then
			DISCQUART=0
			echo "$MissErrorDiscReply"
		elif ! [[ $DISCQUART = `lshw -class disk | grep /dev/$DISCQUART | cut -d ':' -f 2 | cut -d '/' -f 3` ]]
		then
			DISCQUART=0
			echo "$MissErrorDiscFound"
		else
			break
		fi
	done
fi
#------------------------------------------------------------------------------------------




#------------------------------------------------------------------------------------------
#--- Si no se ha escollit mode comprovació, comprova que existeixin els discs assignats ---
#---	per defecte -----------------------------------------------------------------------
#Si el mode comprovació està desactivat llavors procedirem a revisar si els discs introduïts
#	al softcode son vàlids, això ho farem amb l'eïna "fdisk -l".
if (( $AUX == 0 && $COMPR == 1 ))
then
	printf " -> Mode comprovació de discs desactivat, procedint a evaluar discs per defecte:\n"
	#Un for per cada disc, admet 4 valors i defineix la i com a cadascun d'ells.
	for i in $DISCPRIMER $DISCSEGON $DISCTERCER $DISCQUART
	do
		printf "	$i... "
		#Un fdisk amb retallades per saber si correspon al disc introduït, en cas contrari
		#	donarà un error i es cancel·larà el programa.
		if ! [[ `fdisk -l | grep "$i" | head -n 1 | cut -d '/' -f '3' | cut -d ':' -f 1` = "$i" ]]
		then
			printf "error.\n"
			AUX=8
			UNITAT=$i
			break
		else
			printf "OK.\n"
		fi
	done
fi
#------------------------------------------------------------------------------------------




#------------------------------------------------------------------------------------------
#--- Comprovació si tenen suficient capacitat els discs -----------------------------------
#En aquest módul es comprova que cada disc tingui la capacitat total adequada,
#	sense aquesta comprovació potser que a mitja operació hi hagin errors per
#	insuficiència d'espai al disc dur.
if (( $AUX == 0 ))
then
	printf " -> Comprovació de la capacitat dels discs:\n"
	for i in $DISCPRIMER $DISCSEGON $DISCTERCER $DISCQUART
	do
		printf "	$i... "
		#Agafem els octets que es solen mostrar per pantalla amb el fdisk -l i els comparem
		#	amb la variable del softcode. Si qualsevol dels discs no té capacitat suficient
		#	l'script es cancel·la amb el seu missatge d'error corresponent.
		if ! (( `fdisk -l | grep "$i" | head -n 1 | cut -d ',' -f 2 | cut -d ' ' -f 2` > $TAMANYMAXIM ))
		then
			printf "error.\n"
			AUX=6
			#També volem saber on hi està l'error.
			UNITAT=$i
			break
		else
			printf "OK.\n"
		fi
	done
fi
#------------------------------------------------------------------------------------------




#------------------------------------------------------------------------------------------
#--- Eliminació i creació de tables de partició per cada disc -----------------------------
#Cada vegada que executem l'script farem taules de partició noves, amb això ens evitem
#	que un disc tingui una partició a sdc2 la qual, sigui incompatible amb aquest
#	programa, també depurem posibles errors que hi poden haver-hi en el disc.
if (( $AUX == 0 ))
then
	printf " -> Creant taules de partició i particions per cada disc:\n"
	for i in $DISCPRIMER $DISCSEGON $DISCTERCER $DISCQUART
	do
		printf "	$i... Esborrant taula... \n"
		#No volem que pregunti res, llavors farem 2 echos i els hi pasarem al programa
		#	fdisk, el cual esborrarà la taula de particions i crearà una de nova.
		(echo o; echo w) | fdisk /dev/"$i" &> /dev/null
		printf "	Creant taula de particions i creant particions.\n"
		#Aquí crearem una nova partició primària a la posició 1 i guardarem els canvis.
		(echo n; echo p; echo 1; echo ; echo ; echo w) | fdisk /dev/"$i" &> /dev/null
	done
fi
#------------------------------------------------------------------------------------------





#------------------------------------------------------------------------------------------
#--- Comprovació si existeix la carpeta temporal ------------------------------------------
#Si no existeix la carpeta temporal no podrem depositar els arxius on guardarem la
#	informació, que és de vital importància ja que l'script està destinat a això.
if (( $AUX == 0 ))
then
	printf " -> Comprovant carpeta temporal... "
	#Comprovem que existeixi, del contrari mostrarà un missatge d'error i
	#	l'execució del programa es cancel·larà.
	if ! [ -d "$RUTATMP" ]
	then
		printf "error.\n"
		AUX=5
	#Si existeix la carpeta temporal "../Prova" la esborrarem i crearem una de nova
	#	per a que no es barrejin els resultats.
	elif [ -d "$RUTATMP/$DIRACREARTMP" ]
	then
		printf "Esborrant i creant-la de nou... "
		rm -R "$RUTATMP/$DIRACREARTMP"
		mkdir "$RUTATMP/$DIRACREARTMP"
	#I si no existeix, crea-la.
	else
		printf "Creant-la... "
		mkdir "$RUTATMP/$DIRACREARTMP"
	fi
	printf "OK.\n"
fi
#------------------------------------------------------------------------------------------




#------------------------------------------------------------------------------------------
#--- Creació de les carpetes de muntatge --------------------------------------------------
#--- Arxiu temporal per excés d'arguments -------------------------------------------------
#--- Comprovació si particions estàn o no muntades ----------------------------------------
#Contemplem la possibilitat de que les carpetes de muntatge no existeixin, o
#	fins i tot que qualsevol dispositiu estigui muntat en alguna d'aquestes
#	carpetes, si aquest és el cas posiblement dongui un error a mitja operació,
#	aquest módul evita aquesta situació.
if (( $AUX == 0 ))
then
	RUTATMPVARARX="$RUTATMP"/"$DIRACREARTMP"/"$TMPVARARX"
	printf " -> Creant arxiu temporal de variables... "
	#Creem l'arxiu de variables on es depositaràn els sistemes de fitxers
	#	definits a principi de hardcode, només es depositaràn els noms.
	touch "$RUTATMPVARARX"
	printf "OK.\n -> Comprovació de carpetes de muntatge:\n"
	#Per cada sistema de fitxers.
	for i in $FS1 $FS2 $FS3 $FS4
	do
		printf " -$i...\n"
		#Definim el nom de la carpeta on es muntaràn els discs.
		RUTAFS="$1"/"$i"_FS
		#Depositem a l'arxiu de variables el nom de cada FS.
		echo "$i" >> "$RUTATMPVARARX"
		printf "	Variable depositada.\n"
		#Comprovem si existeix la carpeta de muntatge.
		if [ -d "$RUTAFS" ]
		then
			printf "	Carpeta de muntatge detectada.\n"
			#Si existeix comprovarem si està muntada, sabem que si possem
			#	"mountpoint 'ruta'" ens retornarà un:
			#	... is a mountpoint. en cas de que estigui cap cosa allà muntada
			#	sent aquest el cas, desmuntarà aqiesta ruta.
			if [[ `mountpoint "$RUTAFS"` = "$RUTAFS is a mountpoint" ]]
			then
				printf "	Carpeta muntatge desmuntada.\n"
				umount "$RUTAFS"
			fi
			printf "	Carpeta de muntatge esborrada ($RUTAFS).\n"
			#Borrem la carpeta amb el seu contingut.
			rm -R "$RUTAFS"
		fi
		#Creem la carpeta de muntatge on es muntarà el corresponent disc.
		mkdir "$RUTAFS"
		printf "	Carpeta de muntatge creada ($RUTAFS).\n"
	done
fi
#------------------------------------------------------------------------------------------




#------------------------------------------------------------------------------------------
#--- Començament de la funció de l'script -------------------------------------------------
if (( $AUX == 0 ))
then
	AUXRUTA="$RUTATMP/$DIRACREARTMP"
	#Farem un for per cada mida de clúster.
	for i in $PETITCLUSTER $MITJACLUSTER $GRANCLUSTER
	do
		#Si no existeix la carpeta la crea, aquesta ruta és on es depositaràn les dades
		#	amb aquesta mida de clúster.
		if ! [ -d "$AUXRUTA"/Cluster"$i"B ]
		then
			mkdir "$AUXRUTA"/Cluster"$i"B
		fi
		if ! [ -e "$AUXRUTA/$CLTIME" ]
		then
			touch "$AUXRUTA/$CLTIME"
		fi
		printf "\n$MissIntrTmpClust $i\n" >> "$AUXRUTA"/"$CLTIME"
		AUXTMPCLUSTERS="$AUXRUTA"/Cluster"$i"B
		#Executem l'scrip Core.sh que contindrà les execucions dels sistemes de fitxers.
		#Recordem:
		# - $DISCPRIMER 	 ->  $1  -> primer disc físic (ex. "sdb")
		# - $DISCSEGON		 ->	 $2  -> segon disc físic (ex. "sdc")
		# - $DISCTERCER 	 ->  $3  -> tercer disc físic (ex. "sdd")
		# - $DISCQUART  	 ->  $4  -> quart disc físic (ex. "sde")
		# - $RUTA		 	 ->  $5  -> ruta on es faràn les proves (ex. "/media")
		# - $i				 ->  $6  -> mida del cluster (ex. "1024")
		# - $AUXTMPCLUSTERS	 ->	 $7  -> carpeta tmp del clúster actual (ex. "/tmp/Prova/Cluster1024B")
		# - $RUTATMPVARARX	 ->  $8  -> arxiu de variables (ex. "/tmp/Prova/variables")
		/usr/bin/time -p -a -o "$AUXRUTA/$CLTIME" bash -c "./Core.sh $DISCPRIMER $DISCSEGON $DISCTERCER $DISCQUART $RUTA $i $AUXTMPCLUSTERS $RUTATMPVARARX"
	done
#------------------------------------------------------------------------------------------
#--- Missatges d'error i cancel·lament del programa ---------------------------------------
elif (( $AUX == 1 ))
then
	echo "$MissErrorCancel" #	Error, carpeta especificada no trobada.
elif (( $AUX == 2 ))
then
	echo "$MissErrorPermis" #	Error, no has executat aquest script com a sudo.
elif (( $AUX == 3 ))
then
	echo "$MissErrorMissingScript $ErrNOMARXIU" #	Error, no existeix el següent fitxer script: Core.sh
elif (( $AUX == 4 ))
then
	echo "$MissErrorLSHW" #		Error, no està instal·lat el programari lshw, per a instalar-ho executa: sudo apt-get install lshw
elif (( $AUX == 5 ))
then
	echo "$MissErrorDirTmp" #	Error, la carpeta per ubicar fitxers de temps no existeix.
elif (( $AUX == 6 ))
then
	echo "$MissErrorEspai $UNITAT" #	Error, el disc no té la suficient capacitat per a l'execució de l'script: sdb
elif (( $AUX == 7 ))
then	
	echo "$MissErrorPermisFile $ErrNOMARXIU" #	  Error, no hi han permisos d'execució en el següent fitxer: Core.sh
elif (( $AUX == 8 ))
then
	echo "$MissErrorDiscDef $UNITAT" #	Error, disc assignat per defecte inexistent: sdb
elif (( $AUX == 9 ))
then
	echo "$MissErrorProgsFS $Program" #		Error, no hi està instalat un programa indispensable: xfsprogs
fi
#------------------------------------------------------------------------------------------
