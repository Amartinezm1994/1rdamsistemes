#! /bin/bash

#AUTORS:
# -Adrian Mart�nez
# -N�ria Alcal�
# -Ismael Soriano
# -Nil Llisterri

#Arguments passats a aquest script:
# - $1 -> sistema de fitxers (ex. "NTFS")
# - $2 -> disc f�sic (ex. "sdb")
# - $3 -> ruta on es far�n les proves (ex. "/media")
# - $4 -> mida del cluster (ex. "1024")
# - $5 -> ruta on es depositar�n els fixters temporals d'aquest FS (ex. "/tmp/Prova/Cluster1024B/NTFS")

#Nom arxius & arguments:
# ./Formateig.sh "ruta" "cluster size" "ruta temporal" "ruta disc fisic" "FS"
# ./Petits.sh "ruta" "ruta temporal"
# ./System.sh "ruta" "ruta temporal"
# ./Grans.sh "ruta" "ruta temporal" "disc f�sic"

#----------------------------
#---------SOFTCODE-----------
#----------------------------

TMPARX="total$1.txt" #Nom d'arxiu on es depositar�n els temps totals

#----------------------------
#---------INFOCODE-----------
#----------------------------

#---Introducci� de dades en arxius d'informaci�
MissIntrTmpForm="---Temps total per la formataci� en: $1"
MissIntrTmpCrPet="---Temps total per les operacions de fitxers petits en: $1"
MissIntrTmpOSys="---Temps total per les operacions amb arxius de sistema en: $1"
MissIntrTmpCrGra="---Temps total per les operacions amb fitxers grans en: $1"

#----------------------------
#---------HARDCODE-----------
#----------------------------

RUTAMUNT="$3"/"$1"_FS
RUTATMPARX="$5"/"$TMPARX"
RUTADISC=/dev/"$2"1

printf "\n-------------- Sistema de fitxers: $1 --------------\n"

#Creem l'arxiu con depositarem els temps.
touch "$RUTATMPARX"

#Execuci� de la formataci� d'aquest FS en aquest disc f�sics (ex. NTFS - sdb)
printf "\n$MissIntrTmpForm\n" >> "$RUTATMPARX"
COMMAND="./Formateig.sh $3 $4 $2 $1"
/usr/bin/time -p -a -o "$RUTATMPARX" bash -c "$COMMAND"

#Execuci� de les operacions amb arxius petits.
printf "\n$MissIntrTmpCrPet\n" >> "$RUTATMPARX"
COMMAND="./Petits.sh $RUTAMUNT $5"
/usr/bin/time -p -a -o "$RUTATMPARX" bash -c "$COMMAND"

#Execuci� de les operacions amb arxius de sistema.
printf "\n$MissIntrTmpOSys\n" >> "$RUTATMPARX"
COMMAND="./System.sh $RUTAMUNT $5"
/usr/bin/time -p -a -o "$RUTATMPARX" bash -c "$COMMAND"

#Execuci� de les operacions amb arxius grans.
printf "\n$MissIntrTmpCrGra\n" >> "$RUTATMPARX"
COMMAND="./Grans.sh $RUTAMUNT $5 $2"
/usr/bin/time -p -a -o "$RUTATMPARX" bash -c "$COMMAND"

#Desmuntem la partici� en q�esti�.
printf "\n	Desmuntant $21 de $RUTADISC\n"
umount "$RUTADISC"
printf "	Disc desmuntat.\n"
