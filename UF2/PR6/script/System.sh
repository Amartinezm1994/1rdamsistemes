#!/bin/bash
#AUTORS:
# -Adrian Mart�nez
# -N�ria Alcal�
# -Ismael Soriano
# -Nil Llisterri

# ./System.sh "ruta" "ruta temporal" "FS" �"ruta a copiar"?

#Arguments passats a aquest script:
# - $1 -> ruta a enganxar(ex. "/media/NTFS_FS")
# - $2 -> Sistema de fitxers (ex. "NTFS")

#VARIABLES (POTS MODIFICAR)

RUTAAENGANXAR="$1"/System

#PART FIXA (NO TOQUIS!)
RUTAARXTMP="$2"/dadesSystem.txt

mkdir "$RUTAAENGANXAR"


#---------------------------------
# Crea
#---------------------------------
printf "\n	Copiant arxius de sistema.\n"
echo "Temps per copiar els arxius de sistema al disc dur" > "$RUTAARXTMP"
/usr/bin/time -p -a -o "$RUTAARXTMP" bash -c "Parts_sys/./Copia.sh $RUTAAENGANXAR"
printf "	Arxius de sistema copiats.\n"
#---------------------------------



#---------------------------------
# Mou
#---------------------------------
printf "\n	Movent arxius de sistema.\n"
echo "Temps per moure els arxius de sistema al disc dur" >> "$RUTAARXTMP"
/usr/bin/time -p -a -o "$RUTAARXTMP" bash -c "Parts_sys/./Mou.sh $1 $RUTAAENGANXAR"
printf "	Arxius de sistema moguts.\n"
#---------------------------------



#---------------------------------
# Esborra
#---------------------------------
printf "\n	Esborrant arxius de sistema.\n"
echo "Temps per esborrar els arxius de sistema al disc dur" >> "$RUTAARXTMP"
/usr/bin/time -p -a -o "$RUTAARXTMP" bash -c "Parts_sys/./Borra.sh $1"
printf "	Arxius de sistema esborrats.\n"
#---------------------------------

