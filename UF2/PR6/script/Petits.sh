#! /bin/bash
#AUTORS:
# -Adrian Martínez
# -Núria Alcalà
# -Ismael Soriano
# -Nil Llisterri


#VARIABLES (POTS MODIFICAR)
NOMTMP="$2"/"dadesPetits.txt"


#PART FIXA (NO TOQUIS!)
mkdir "$1"/Petits
touch "$NOMTMP"

#-------------------------------------
#----------Crea
printf "\n	S'estàn creant els arxius\n"
echo "Temps de creació de 100MB de fitxers de 10KB" >> "$NOMTMP"
/usr/bin/time -p -a -o "$NOMTMP" bash -c "Parts_petits/./Crea.sh $1/Petits"
printf "	S'han creat els arxius.\n"
#-------------------------------------



#-------------------------------------
#----------Copia
printf "\n	S'estàn copiant els arxius.\n"
echo "Temps per a copiar 10240 fitxers de 10KB" >> "$NOMTMP"
/usr/bin/time -p -a -o "$NOMTMP" bash -c "Parts_petits/./Copia.sh $1"
printf "	S'han copiat els arxius.\n"
#-------------------------------------




#-------------------------------------
#----------Mou
printf "\n	S'estàn movent els arxius\n"
echo "Temps per moure 10240 fitxers de 10KB" >> "$NOMTMP"
/usr/bin/time -p -a -o "$NOMTMP" bash -c "Parts_petits/./Mou.sh $1"
printf "	S'han mogut els arxius.\n"
#-------------------------------------



#-------------------------------------
#----------Mou
printf "\n	S'estàn esborrant els arxius\n"
echo "Temps d'esborrat de 10240 fitxers de 10KB" >> "$NOMTMP"
/usr/bin/time -p -a -o "$NOMTMP" bash -c "Parts_petits/./Borra.sh $1"
printf "	S'han esborrat els arxius\n"
#-------------------------------------
