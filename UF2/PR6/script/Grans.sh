#! /bin/bash
#
# Crea 3GB de dades amb arxius de 500MB i pren la mida de blocs
# ocupats amb l'ordre df
#
#AUTORS:
# -Adrian Martínez
# -Núria Alcalà
# -Ismael Soriano
# -Nil Llisterri
#
#############
# Variables #
#############
RUTA="$1"
ARXIU="$2"/"dadesGrans.txt"
ORIGINAL="Original"
COPIA="Copiat"
MOURE="Mogut"
DISC="$31"

############
#  Script  #
############

#---------------------------------
# Crea
#---------------------------------
printf "\n	Creant carpeta $ORIGINAL...\n"
if [ -d "$RUTA/$ORIGINAL" ]; then
	printf "	Carpeta trobada!\n	Esborrant carpeta..\n"
	rm -R "$RUTA"/"$ORIGINAL"
	mkdir "$RUTA"/"$ORIGINAL"
else
	mkdir "$RUTA"/"$ORIGINAL"
fi
printf "	Carpeta $ORIGINAL creada\n"

printf "	Creant fitxers a la carpeta $ORIGINAL...\n"
printf "* Creació\n\n" > "$ARXIU"
COMMAND="./Parts_grans/Crea.sh $ARXIU $RUTA/$ORIGINAL $DISC"
/usr/bin/time -pa -o"$ARXIU" bash -c "$COMMAND"
printf "	Fitxers creats...\n"
#---------------------------------




#---------------------------------
# Copia
#---------------------------------
printf "\n	Creant carpeta $COPIA...\n"
if [ -d "$RUTA/$COPIA" ]; then
	printf "	Carpeta trobada!\n	Esborrant carpeta..\n"
	rm -R "$RUTA"/"$COPIA"
	mkdir "$RUTA"/"$COPIA"
else
	mkdir "$RUTA"/"$COPIA"
fi
printf "	Carpeta $COPIA creada\n"

printf "	Copiant fitxers de la carpeta $ORIGINAL...\n"
printf "\n* Copia\n\n" >> "$ARXIU"
COMMAND="./Parts_grans/Copia.sh $ARXIU $RUTA $ORIGINAL $COPIA $DISC"
/usr/bin/time -pa -o"$ARXIU" bash -c "$COMMAND"
printf "	Fitxers copiats\n"
#---------------------------------



#---------------------------------
# Mou
#---------------------------------
printf "\n	Creant carpeta $MOURE...\n"
if [ -d "$RUTA/$MOURE" ]; then
	printf "	Carpeta trobada!\n	Esborrant carpeta..\n"
	rm -R "$RUTA"/"$MOURE"
	mkdir "$RUTA"/"$MOURE"
else
	mkdir "$RUTA"/"$MOURE"
fi
printf "	Carpeta $MOURE creada\n"

printf "	Movent fitxers de la carpeta $ORIGINAL a la carpeta $MOURE...\n"
printf "\n* Mou\n\n" >> "$ARXIU"
COMMAND="./Parts_grans/Mou.sh $ARXIU $RUTA $ORIGINAL $MOURE $DISC"
/usr/bin/time -pa -o"$ARXIU" bash -c "$COMMAND"
echo "	Fitxers moguts"
#---------------------------------



#---------------------------------
# Esborra
#---------------------------------
printf "\n	Esborrant contingut carpeta $RUTA...\n"
printf "\n* Borra\n\n" >> "$ARXIU"
COMMAND="./Parts_grans/Borra.sh $ARXIU $RUTA $DISC"
/usr/bin/time -pa -o"$ARXIU" bash -c "$COMMAND"
printf "	Contingut esborrat\n"
#---------------------------------
