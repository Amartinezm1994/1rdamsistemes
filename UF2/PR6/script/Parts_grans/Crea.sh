#!/bin/bash
#
#AUTORS:
# -Adrian Martínez
# -Núria Alcalà
# -Ismael Soriano
# -Nil Llisterri
#
#############
# Variables #
#############
ARXIU="$1"
CARPETA="$2"
PARTICIO="$3"

#############
#  Script   #
#############
printf "Variacions de mida amb l'ordre df\n" >> "$ARXIU"
printf "				" >> "$ARXIU"
df | grep "fitxers" >> "$ARXIU"
printf "Espai inicial			" >> "$ARXIU"
df | grep "$PARTICIO" >> "$ARXIU"

for i in {1..8}
do
	dd if=/dev/zero of="$CARPETA"/Gran"$i".txt bs=51200k count=10 &> /dev/null
	printf "Espai al afegir el arxiu $i	" >> "$ARXIU"
	df | grep "$PARTICIO" >> "$ARXIU"
done

printf "\nTemps d'execució\n" >> "$ARXIU"
