#!/bin/bash
#
#AUTORS:
# -Adrian Martínez
# -Núria Alcalà
# -Ismael Soriano
# -Nil Llisterri
#
#############
# Variables #
#############
ARXIU="$1"
CARPETA="$2"
PARTICIO="$3"

#############
#  Script   #
#############
printf "Variacions de mida amb l'ordre df\n" >> "$ARXIU"
printf "								" >> "$ARXIU"
df | grep "$PARTICIO" >> "$ARXIU"
printf "Espai inicial							" >> "$ARXIU"
df | grep "$PARTICIO" >> "$ARXIU"

rm -R "$CARPETA"/*
printf "Espai al esborrar el contingut de $CARPETA		" >> "$ARXIU"
df | grep "$PARTICIO" >> "$ARXIU"

printf "\nTemps d'execució\n" >> "$ARXIU"
