#!/bin/bash
#
#AUTORS:
# -Adrian Martínez
# -Núria Alcalà
# -Ismael Soriano
# -Nil Llisterri
#
#############
# Variables #
#############
ARXIU="$1"
RUTA="$2"
ORIGEN="$3"
DESTI="$4"
PARTICIO="$5"

#############
#  Script   #
#############
printf "Variacions de mida amb l'ordre df\n" >> "$ARXIU"
printf "							" >> "$ARXIU"
df | grep "fitxers" >> "$ARXIU"
printf "Espai inicial						" >> "$ARXIU"
df | grep "$PARTICIO" >> "$ARXIU"

cp -R "$RUTA"/"$ORIGEN"/* "$RUTA"/"$DESTI"
printf "Espai al copiar el contingut de $ORIGEN en $DESTI	" >> "$ARXIU"
df | grep "$PARTICIO" >> "$ARXIU"

printf "\nTemps d'execució\n" >> "$ARXIU"
