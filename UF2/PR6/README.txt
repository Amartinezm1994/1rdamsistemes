#----------------------------------------------------------
#-------------------DESCRIPCI� PAS A PAS-------------------
#----------------------------------------------------------
# Script creat amb la finalitat de comparar sistemes de fitxers.
# L'script en si t� comprovacions de tota mena amb missatges
#   descriptius que indica a l'usuari el progr�s.
# Script estructurat en blocs de comprovaci�, si falla una
#	comprovaci� se'n va cap al final i mostra l'error.
# Fem un time de tot, el que volem �s obtenir totes les dades
#	posibles.
# NOTA: descripci� completa a cada linia de cada fitxer script.
# Breu descripci� de cada fitxer:
#----------------------------------

#----------------------------------
#-Main.sh
#	Execuci�: sudo ./Main.sh /media
#		|__ ./Main.sh 		<-- Script
#		|__ "/media" = $1 	<-- Ruta on far� les operacions.
#
#---------
#- Objectius de "Main.sh"
#	Comprovar que tot estigui en ordre i que en mitj proc�s de
#	comparaci� no sorgeixin errors que ocasionin una inconsist�ncia
#	de dades. Volem que les dades siguin fiables per tant deuen
#	haver-hi comprovacions.
#---------
#- 1 -Comprova si �s root.
#- 2 -Comprova carpeta passada com argument.
#- 3 -Comprova components de l'script i permissos.
#- 4 -Comprova programes de formataci� REISER i XFS.
#- 5 -Mode selecci� de discs o no.
#	R: Si - Comprova que programari LSHW estigui instalat.
#	R: No - Assigna els discs al per defecte.
#- 6 -R: Si: Demana discs a l'usuari i els va comprovant.
#- 7 -R: No: Comprova que els discs per defecte existeixin.
#- 8 -Comprova que tinguin suficient capacitat els discs.
#- 9 -Eliminaci� i creaci� de taules de particions a cada disc.
#-10 -Comprovaci� d'exist�ncia de carpeta temporal.
#-11 -Comprovaci� i creaci� de carpetes de Muntatge.	-\
#-12 -Creaci� d'un arxiu temporal amb variables.		-|	--> Mateix bloc
#-13 -Comprovaci� si hi han particions muntades.		-/
#-14 -For per cada mida de cl�ster executant el seg�ent script: Core.sh
#-15 -Missatges d'error basats en el n�mero de la variable AUX.
#----------------------------------

#----------------------------------
#-Core.sh
#	Execuci�: ./Core.sh sdb sdc sdd sde /media 1024 /tmp/Prova/Cluster1024B /tmp/Prova/variables
#		|_ ./Core.sh 		<-- Script
#		|_ "sdb" = $1 		<-- Primer disc f�sic
#		|_ "sdc" = $2 		<-- Segon disc f�sic
#		|_ "sdd" = $3		<-- Tercer disc f�sic
#		|_ "sde" = $4 		<-- Quart disc f�sic
#		|_ "/media" = $5 	<-- Ruta on es far�n les operacions
#		|_ "1024" = $6		<-- Mesura de cl�ster, determinat pel for
#		|_ "/tmp/Prova/Cluster1024B" = $7	<-- Carpeta temporal, es depositar� tota la informaci� amb aquesta mida de cl�ster
#		|_ "/tmp/Prova/variables" = $8		<-- Arxiu on es depositen les variables dels FSs
#						|_ "NTFS"	-\
#						|_ "EXT4"	-|	-> Sistemes de fitxers.
#						|_ "XFS"	-|
#						|_ "REISER" -/
#
#---------
#- Objectius de "Core.sh"
#	Executa l'arxiu que realitza les operacions amb arxius,
#	una vegada per cada FS.
#	Omet les operacions d'un determinat FS amb una mida de
#	cl�ster incompatible amb ell.
#---------
#- 1 -Defineix variables.
#- 2 -Comen�a el for, variable i per cada sistema de fitxers.
#- 3 -Declarem variables amb punter per redu�r el codi.
#- 4 -Comprovacions de mida de cl�ster per ometre combinacions incorrectes de FSs i cl�sters.
#- 5 -Execuci� de: Execution.sh
#----------------------------------

#----------------------------------
#-Execution.sh
#	Execuci�: ./Execution.sh NTFS sdb /media 1024 /tmp/Prova/Cluster1024B/NTFS
#		|_ ./Execution.sh		<-- Script
#		|_ "NTFS" = $1			<-- Sistema de fitxers
#		|_ "sdb" = $2			<-- Disc f�sic on es far�n operacions
#		|_ "/media" = $3		<-- Ruta on es far�n les operacions
#		|_ "1024" = $4			<-- Mida de cl�ster
#		|_ "/tmp/Prova..." = $5	<-- Carpeta temporal, es depositar� tota la informaci� del FS otorgat
#
#---------
#- Objectius de "Execution.sh"
#	Executa totes les operacions de formateig i fitxers,
#	m�s concretament, formateja, despr�s executa les
#	ordres de fitxers petits, seguidament de les de
#	fitxers de sistema i per �ltim les de fitxers
#	grans, finalment desmunta aquesta partici�.
#---------
#- 1 -Defineix variables.
#- 2 -Executa Formateig.sh
#- 3 -Executa Petits.sh
#- 4 -Executa System.sh
#- 5 -Executa Grans.sh
#- 6 -Desmunta partici�.
#----------------------------------

#----------------------------------
#-Formateig.sh
#	Execuci�: ./Formateig.sh /media 1024 sdb NTFS
#		|_ ./Formateig.sh		<-- Script
#		|_ "/media" = $1		<-- Carpeta on es muntar� el disc
#		|_ "1024" = $2			<-- Mida de cl�ster
#		|_ "sdb" = $3			<-- Disc f�sic
#		|_ "NTFS" = $4			<-- Sistema de Fitxers
#
#---------
#- Objectius de "Formateig.sh"
#	Formateja i munta. Segons unes condicions o d'altres formatejar�
#	amb unes ordres o altres, dep�n de FS/Cl�ster, l'objectiu final
#	d'aquest subscript �s fer una correcta formataci� i posteriorment
#	poder muntar-hi la partici� correctament. Sense aquestes dues
#	condicions hi han moltes possibilitats de que hi hagi una
#	fallada al fer-hi les operacions.
#---------
#- 1 -Defineix variables.
#- 2 -Comprova sistema de fixters/cl�sters
#- 3 -Executa la formataci�.
#- 4 -Munta el disc formatat.
#----------------------------------

#----------------------------------
#-Petits.sh
#	Execuci�: ./Petits.sh /media/NTFS_FS /tmp/Prova/Cluster1024B/NTFS
#		|_ ./Petits.sh				<-- Script
#		|_ "/media/NTFS_FS" = $1	<-- Carpeta on es far�n les operacions
#		|_ "/tmp/.../NTFS" = $2		<-- Carpeta on es depositar�n els temps
#
#---------
#- Objectius de "Petits.sh"
#	Crea, copia, mou i esborra. Totes aquestes operaciones les fa
#	amb arxius de mida petita, en total ~100MB total de dades, vem
#	decidir fer-ho a aquesta mida perqu� trigava molt�sim a fer
#	els arxius petits en, per exemple, 3GB de dades. 
#---------
#- 1 -Defineix variables.
#- 2 -Executa Crea.sh
#				|_ #--------------------------------------------
#				   # ->  ./Parts_petites/Crea.sh
#				   #----
#				   #-Executa la creaci� de fitxers.
#				   #	Inclou un for que es repeteix un
#				   #	m�ltiple n�mero de vegades per a crear
#				   #	100MB de dades,
#				   #--------------------------------------------
#
#- 3 -Executa Copia.sh
#				|_#--------------------------------------------
#				  # ->	./Parts_petites/Copia.sh
#				  #----
#				  #-Executa la c�pia de fitxers.
#				  #		Inclou un cp recursiu de ./Petits/*
#				  #		a una altre carpeta dest� en el
#				  #		punt de muntatge.
#				  #--------------------------------------------
#
#- 4 -Executa Mou.sh
#				|_#--------------------------------------------
#				  # ->	./Parts_petites/Mou.sh
#				  #----
#				  #-Mou els fitxers creats.
#				  #		T� un for que va movent arxiu a
#				  #		arxiu a una altre carpeta creada
#				  #		per aquest subscript.
#				  #--------------------------------------------
#
#- 5 -Executa Borra.sh
#				|_#--------------------------------------------
#				  # ->	./Parts_petites/Borra.sh
#				  #----
#				  #-Esborra tots els fitxers creats.
#				  #		Fa un rm -R de tota la carpeta de
#				  #		muntatge.
#				  #--------------------------------------------
#----------------------------------

#----------------------------------
#-System.sh
#	Execuci�: ./System.sh /media/NTFS_FS /tmp/Prova/Cluster1024B/NTFS
#		|_ ./System.sh				<-- Script
#		|_ "/media/NTFS_FS" = $1	<-- Carpeta on es far�n les operacions
#		|_ "/tmp/.../NTFS" = $2		<-- Carpeta on es depositar�n els temps
#
#---------
#- Objectius de "System.sh"
#	Copia, mou i esborra. Fa aquestes funcions amb els arxius
#	del sistema, carpeta "/", exclou diverses carpetes per evitar
#	errors.
#---------
#- 1 -Defineix variables.
#- 2 -Executa Copia.sh
#				|_#--------------------------------------------
#				  # ->	./Parts_sys/Copia.sh
#				  #----
#				  #-Executa la c�pia de fitxers de sistema
#				  #		Inclou un cp recursiu de / exclo�nt les
#				  #		seg�ents carpetes: /media, /proc, /usr,
#				  #		/var, /sys. El motiu �s que no finalitzaba
#				  #		mai el proc�s de copia, sense aquestes
#				  #		fa el temps c�pia relativament curt.
#				  #--------------------------------------------
#
#- 3 -Executa Mou.sh
#				|_#--------------------------------------------
#				  # ->	./Parts_sys/Mou.sh
#				  #----
#				  #-Mou els fitxers creat.
#				  #		Mou tot el contigut d'on s'han copiat els
#				  #		a una d'altre.
#				  #--------------------------------------------
#
#- 4 -Executa Borra.sh
#				|_#--------------------------------------------
#				  # ->	./Parts_sys/Borra.sh
#				  #----
#				  #-Esborra tots els fitxers copiats.
#				  #		Fa un rm -R de tota la carpeta de
#				  #		muntatge.
#				  #--------------------------------------------
#----------------------------------

#----------------------------------
#-Grans.sh
#	Execuci�: ./Grans.sh /media/NTFS_FS /tmp/Prova/Cluster1024B/NTFS sdb
#		|_ ./Grans.sh				<-- Script
#		|_ "/media/NTFS_FS" = $1	<-- Carpeta on es far�n les operacions
#		|_ "/tmp/.../NTFS" = $2		<-- Carpeta on es depositar�n els temps
#		|_ "sdb" = $3				<-- Disc f�sic
#
#---------
#-Objectius de "Grans.sh"
#	Crea, copia, mou i esborra. Fa totes aquestes operacions
#	amb arxius d'una mida de ~500MB fins a 3GB. A m�s
#	d'enregistrar el temps, tamb� mesura l'espai en disc
#	amb l'ordre df.
#---------
#- 1 -Defineix variables.
#- 2 -Executa Crea.sh
#				|_ #--------------------------------------------
#				   # ->  ./Parts_grans/Crea.sh
#				   #----
#				   #-Executa la creaci� de fitxers.
#				   #	Inclou un for que es repeteix 8 vegades
#				   #	amb fitxers de 500MB, arriba fins als
#				   #	3GB d'espai.
#				   #--------------------------------------------
#
#- 3 -Executa Copia.sh
#				|_#--------------------------------------------
#				  # ->	./Parts_grans/Copia.sh
#				  #----
#				  #-Executa la c�pia de fitxers.
#				  #		Inclou un cp recursiu de ./Grans/*
#				  #		a una altre carpeta dest� en el
#				  #		punt de muntatge.
#				  #--------------------------------------------
#
#- 4 -Executa Mou.sh
#				|_#--------------------------------------------
#				  # ->	./Parts_grans/Mou.sh
#				  #----
#				  #-Mou els fitxers creats.
#				  #		T� un mv recursiu fins a una carpeta de
#				  #		dest� creada per l'script.
#				  #--------------------------------------------
#
#- 5 -Executa Borra.sh
#				|_#--------------------------------------------
#				  # ->	./Parts_grans/Borra.sh
#				  #----
#				  #-Esborra tots els fitxers copiats.
#				  #		Fa un rm -R de tota la carpeta de
#				  #		muntatge.
#				  #--------------------------------------------
#----------------------------------

#******************************************************
#******************** Limitacions *********************
#******************************************************
#* 1 -No es poden modificar des de les variables el 
#*    tipus de FS per comparar per la nomenclatura
#*    al fer una formataci� amb el programa o per
#*    posibles programes requerits (per� si d'ordre).
#* 
#* 2 -Mida total de creaci� de fitxers no es pot modificar.
#*
#* 3 -No es poden modificar els noms dels fitxers que
#*    conformen l'script.
#*
#* 4 -No �s ben b� una limitaci� d'aquest programa per� si
#*	  dels FSs i �s que segons quina mesura de cl�ster no
#*	  et far� la operaci� desitjada, si no que aquest programa
#*	  far� comprovacions per saber si no donar� un error
#*	  posteriorment.
#*
#* 5 -No es poden afegir m�s de tres mesures de cl�ster
#*	  per� si es pot executar aquest programa amb diferents
#*	  mesures de cl�ster cada vegada.
#*
#* 6 -Mida m�xima modificable per� una mica inservible si
#*	  pensem en que la mida m�xima a ocupar al disc �s
#*	  ~6GB per les operacions d'escriptura, que per a modi-
#*	  ficar-les es t� que tocar el hardcode.
#*
#*	7 -Carpeta a copiar del sistema no es pot canviar si no
#*	   �s mitjan�ant hardcode.
#*
#*	8 -No es poden afegir m�s de 4 discs f�sics, si aquest
#*	   f�ra el cas, no faria cas de l'�ltim disc introdu�t.



#***************************************************************
#**************************** FAQ ******************************
#***************************************************************
# Q: Com s'estructuren els directoris de les dades recopilades?
# R: Dins de la carpeta especificada a una variable del Main.sh
#	 es deposita la ruta dins de la carpeta /tmp que tamb� �s
#	 modificable. Dins d'aquesta hi han 3 carpetes amb la mesura
#	 de cl�ster i dins d'aquestes una per cada FS compatible amb
#	 la mesura del cl�ster en q�esti�.
#
# Q: Arxiu de variables?
# R: Si, com que no �s podien passar m�s arguments que 9 si voliem
#	 que l'script funcion�s correctament vem implementar la creaci�
#	 d'un arxiu anomenat "variables" dins de la ruta on es crean
#	 els fitxers d'informaci�, la seva localitzaci� �s en el m�dul
#	 de "Comprovaci� i creaci� de carpetes de muntatge". Aquest
#	 fitxer es llegir� posteriorment en l'script "Core.sh" per fer
#	 les operacions pertinents.
#
# Q: Mode comprovaci�?
# R: En aquest programa hem implementat un petit sistema on podem
#	 escollir els discs on fer les operacions a temps real amb la
#	 certesa de que ser�n correctes. En cas contrari s'agafar�n
#	 els discs per defecte que hi son al softcode. Totalment modi-
#	 ficables, sempre i quan no sigui un disc inv�lid, en aquest
#	 cas donar� un error i es cancel�lar� el programa, aquest
#	 aspecte tamb� ho hem valorat per a poguer possar-lo al
#	 softcode.
#
# Q: Quantes voltes d�na aquest script?
# R: Podem calcular aix� fent:
#	 - 3 mides de cl�ster
#	 - 4 sistema de fitxers
#	 - 4 operacions (Formateja, crea grans, copia sys i crea petits)
#	 - Crea, c�pia, mou i esborra per grans i petits, per sys nom�s
#		c�pia, mou i esborra, per formateig �s 1 nom�s.
#	 3 * 4 * (4 + 4 + 3 + 1) = 144 execucions de fitxers d'script.
#
# Q: Quant temps de mitja triga l'script a executar-se?
# R: Segons les dades obtingudes al timeclusters.txt, amb una mida
#	 de cl�ster est�ndar de 1024,4096 i 16384B trigar� uns 90 minuts
#	 a executar-se tot l'script correctament.