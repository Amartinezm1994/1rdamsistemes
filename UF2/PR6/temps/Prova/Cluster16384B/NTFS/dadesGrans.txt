* Creació

Variacions de mida amb l'ordre df
				S. fitxers         Blocs   1K     En ús   Lliures  %Ús Muntat a
Espai inicial			/dev/sdb1             10482368     73536  10408832   1% /media/NTFS_FS
Espai al afegir el arxiu 1	/dev/sdb1             10482368    585536   9896832   6% /media/NTFS_FS
Espai al afegir el arxiu 2	/dev/sdb1             10482368   1097536   9384832  11% /media/NTFS_FS
Espai al afegir el arxiu 3	/dev/sdb1             10482368   1609536   8872832  16% /media/NTFS_FS
Espai al afegir el arxiu 4	/dev/sdb1             10482368   2121536   8360832  21% /media/NTFS_FS
Espai al afegir el arxiu 5	/dev/sdb1             10482368   2633552   7848816  26% /media/NTFS_FS
Espai al afegir el arxiu 6	/dev/sdb1             10482368   3145552   7336816  31% /media/NTFS_FS
Espai al afegir el arxiu 7	/dev/sdb1             10482368   3657552   6824816  35% /media/NTFS_FS
Espai al afegir el arxiu 8	/dev/sdb1             10482368   4169552   6312816  40% /media/NTFS_FS

Temps d'execució
real 203.04
user 0.07
sys 5.24

* Copia

Variacions de mida amb l'ordre df
							S. fitxers         Blocs   1K     En ús   Lliures  %Ús Muntat a
Espai inicial						/dev/sdb1             10482368   4169552   6312816  40% /media/NTFS_FS
Espai al copiar el contingut de Original en Copiat	/dev/sdb1             10482368   8265568   2216800  79% /media/NTFS_FS

Temps d'execució
real 350.98
user 0.30
sys 7.89

* Mou

Variacions de mida amb l'ordre df
							S. fitxers         Blocs   1K     En ús   Lliures  %Ús Muntat a
Espai inicial						/dev/sdb1             10482368   8265568   2216800  79% /media/NTFS_FS
Espai al copiar el contingut de Original en Mogut	/dev/sdb1             10482368   8265584   2216784  79% /media/NTFS_FS

Temps d'execució
real 0.27
user 0.03
sys 0.00

* Borra

Variacions de mida amb l'ordre df
								/dev/sdb1             10482368   8265584   2216784  79% /media/NTFS_FS
Espai inicial							/dev/sdb1             10482368   8265584   2216784  79% /media/NTFS_FS
Espai al esborrar el contingut de /media/NTFS_FS		/dev/sdb1             10482368     73536  10408832   1% /media/NTFS_FS

Temps d'execució
real 0.30
user 0.02
sys 0.01
