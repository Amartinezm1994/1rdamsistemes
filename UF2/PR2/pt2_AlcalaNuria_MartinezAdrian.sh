#! /bin/bash

#DESCRIPCIÓ:
#Ha de crear 3 GB de fitxers.
#Els fitxers han de tenir mides diferents
#Cap fitxer pot superar els 100MB
#L'script també ha de crear directoris i subdirectoris.
#L'script ha de rebre un paràmetre $1 que ha de ser el directori arrel a partir del qual ha de crear els 3GB de dades
#Ha de tenir comentaris, com a mínim el nom dels components del grup al principi
#A pantalla no ha de mostrar res, excepte un missatge de que comença i un de que acava

#AUTORS:
#Núria Alcalà
#Adrian Martínez

#------IMPORTANT!! -> És recomanable crear una nova carpeta per a aquesta ruta.

#------------------
#----SOFTCODE------
#------------------

#Missatge de començament
MissatgeCom="Comença la creació de fitxers."
MissatgeFin="Creació de fitxers finalitzat."
MissatgeErr="El directori especificat no existeix."

#Variables dels fitxers - Ordre ascendent
F2KB=$1/Fitxers2KB/
F5KB=$1/Fitxers5KB/
F10KB=$1/Fitxers10KB/
F20KB=$1/Fitxers20KB/
F50KB=$1/Fitxers50KB/
F100KB=$1/Fitxers100KB/
F200KB=$1/Fitxers200KB/
F500KB=$1/Fitxers500KB/
F1000KB=$1/Fitxers1000KB/

#Noms dels fitxers
KB2=0
KB5=0
KB10=0
KB20=0
KB50=0
KB100=0
KB200=0
KB500=0
KB1000=0

#------------------
#----HARDCODE------
#------------------

#Comprovació de si existeix la carpeta
if [ -d "$1" ]
then
	
	#Missatge de començament
	echo $MissatgeCom
	
	#Agafa la primera dada de la comanda du (et diu l'espai
	#	ocupat per aquesta carpeta) i mentre sigui menys de
	#	3GB ho seguirá fent.
	while (( `du -s $1 | cut -f 1` <= 3000000)) &> /dev/null
	do
		#Demana un random de entre 0 i 8.
		Rand=$((RANDOM%9))
		
		#Segons el random qur surti agafarà un tamany concret
		#	el pròxim arxiu a crear.
		if (( $Rand==0 ))
		then
			ESPAI=2
		elif (( $Rand==1 ))
		then
			ESPAI=5
		elif (( $Rand==2 ))
		then
			ESPAI=10
		elif (( $Rand==3 ))
		then
			ESPAI=20
		elif (( $Rand==4 ))
		then
			ESPAI=50
		elif (( $Rand==5 ))
		then
			ESPAI=100
		elif (( $Rand==6 ))
		then
			ESPAI=200
		elif (( $Rand==7 ))
		then
			ESPAI=500
		else
			ESPAI=1000
		fi
		
		#Si no existeix la carpeta de l'arxiu en concret, crea-la.
		#	La ruta és la definida com a argument /Fitxers + espai + KB
		if ! [ -d $1/Fitxers$ESPAI\KB/ ] &> /dev/null
		then
			mkdir $1/Fitxers$ESPAI\KB &> /dev/null
		fi
		
		#Crea el fitxer del tamany escollit a l'altzar amb un tamany de bloc d'1KB
		#	Crea-lo tantes vegades com sigui hagi sortit a l'atzar.
		#	Anomena-lo amb la variable que és va autoincrementant
		#	la variable és defineix amb la suma de l'string KB + el tamany
		#	escollit a l'atzar.
		dd if=/dev/zero of=$1/Fitxers$ESPAI\KB/$[KB$ESPAI] bs=1024 count=$ESPAI &> /dev/null
		
		#Això serveix per no crear fitxers iguals, és el nom del fitxer,
		#	cicle del while incrementa en 1 aquesta xifra.
		let "KB$ESPAI += 1" &> /dev/null
	done
	
	#Missatge de finalitzat.
	echo $MissatgeFin

#Si no existeix la carpeta pasada com a argument al executar el programa mostra aquest missatge d'error.
else 
	echo $MissatgeErr
fi

