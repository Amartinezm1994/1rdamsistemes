#! /bin/bash

#AUTOR:
#	-Adrian Martínez Montes

#DESCRIPCIÓ:
#	La descripció és des de aquí fins a la secció del softcode.


#----------------------------------------------------
#		Ús de l'script:
#----------------------------------------------------

# Deixo la llista de paràmetres en primer lloc per comoditat:

#	# ./script.sh au nomusuari nomgrup	<- Crea un usuari en el grup especificat.
#	# ./script.sh ag nomgrup			<- Crea un grup.
#	# ./script.sh du nomusuari			<- Esborra un usuari.
#	# ./script.sh dg nomgrup			<- Esborra un grup.

#	# ./script.sh 						<- Fa una instal·lació per defecte, demana dades, només primera vegada.
#	# ./script.sh noIn 					<- Instal·lació automàtica, no demana res, aplica valors per defecte al softcode.

#	# ./script.sh resD nomdomini		<- Reseteja el domini i demana altre si no s'especifica com a segon argument.
#	# ./script.sh resI IP 				<- Reseteja la IP i demana altre si no s'especifica com a segon argument.
#	# ./script.sh resP 					<- Reseteja la password d'administrador i demana una nova

#	# ./script.sh resA 					<- Fa el reset de totes les configuracions i les demana a l'usuari.
#	# ./script.sh resA noIn 			<- Fa el reset de totes les configuracions i agafa totes del softcode.

#	# ./script.sh rm 					<- Elimina tots els fitxers creats per l'script.
#----------------------------------------------------



#----------------------------------------------------
#		Explicació per a administradors de sistema
#----------------------------------------------------

# La primera vegada que iniciem l'script farà una instal·lació a la carpeta
#	que estigui especificada al softcode, podrem possar qualsevol inclús
#	especificar-hi el "." que indica la mateixa, l'script a l'hora de
#	fer les operacions la tindrà en compte.

# L'script es té que executar sempre en superusuari o del contrari l'script
#	detindrà la seva execució i mostrarà un error.

# Qualsevol operació sense instal·lar l'script farà automàticament la
#	instal·lació i després procedirà amb la operació pertinent.

# Podem fer una instal·lació automatitzada, que no demani cap dada i que agafi
#	tot el que hi hagi especificat al softcode.

#	# ./script.s 						<- Fa una instal·lació per defecte,
#											demana dades.
#	# ./script.sh noIn 					<- Instal·lació automàtica, no demana res.

# Cal dir que totes les configuracions una vegada estigui instal·lat l'script
#	les agafarà de l'arxiu de configuració, per tant si volem canviar qualsevol
#	configuració post-instal·lat l'script anirem a aquest arxiu. És important
#	respectar el format de l'arxiu de configuració o podría donar lloc a
#	errors en l'execució de l'script.

# Si per exemple s'ha esborrat qualsevol arxiu de configuració per error l'script
#	ja s'encarrega de crear-lo una altre vegada automàticament, en cas de que
#	sigui l'arxiu password, tornarà a demanar una password.

# La contrasenya d'administrador és allotjada a un arxiu apart en text plà però
#	amb permisos de només lectura per a l'usuari que executa aquest script,
#	o sigui, que només root pot llegir-ho (400).

# Tenim la possibilitat de canviar algun paràmetre només executant l'script
#	i possant algun argument, els possibles són els següents:

#	# ./script.sh resD nomdomini		<- Reseteja el domini i demana altre si no 
#											s'especifica com a segon argument.
#	# ./script.sh resI IP 				<- Reseteja la IP i demana altre si no
#											s'especifica com a segon argument.
#	# ./script.sh resP 					<- Reseteja la password d'administrador
#											i demana una nova.

# Aquestes son tres configuracions que podem reinicar amb l'execució de l'script,
#	però, quines són totes les configuracions que l'script agafa per fer totes
#	les seves operacions? Aquí dóno una llista:

#	- Nom del fitxer on s'allotja la contrasenya de l'administrador LDAP.
#	- Primera ID dels grups que es crearàn.
#	- Primera ID dels usuaris que es crearàn.
#	- Nom de la unitat organitzativa on s'allotjaràn els grups.
#	- Nom de la unitat organitzativa on s'allotjaràn els usuaris.
#	- Nom del domini (només s'accepta subdomini.domini).
#	- IP del servidor LDAP (en format "ldap://localhost").
#	- Nom de l'usuari administrador del servidor LDAP.
#	- Nom de les quatre plantilles utilitzades per fer les operacions LDAP.

# Podem també especificar un reset de tota la configuració amb l'execució de l'script,
#	e inclús un reset automàtic amb els valors per defecte del propi script. Per fer
#	aìxò només tindrem que especificar els següent paràmetres:

#	# ./script.sh resA 					<- Fa el reset de totes les configuracions
#											i les demana a l'usuari.
#	# ./script.sh resA noIn 			<- Fa el reset de totes les configuracions
#											i agafa totes del softcode.

# Totes les configuracions no demanades que si estàn al softcode són les que agafarà
#	per al fitxer de configuració, vaig suposar que no calía demanar absolutament
#	totes les configuracions.

# Podem fer una desinstal·lació de l'script, l'script a l'hora d'esborrar tots els arxius
#	té en compte si s'ha especificat una carpeta o si s'ha especificat la mateixa,
#	en cas d'una o altre si a l'hora de fer l'esborrat dels arxius, la carpeta especificada
#	no té més arxius l'esborra per no deixar residus. L'execució d'aquest mode és:

#	# ./script.sh rm 					<- Elimina tots els fitxers creats per l'script.

# Per últim, la funció real de l'script és crear, esborrar un usuari i crear i esborrar
#	un grup. Per fer això es fan servir plantilles LDIF, és a dir, per cada operació
#	(esmentades fa un moment) utilitzarà una plantilla diferent. Però clar, les utilitza,
#	però això no vol dir que tinguerem que configurar aquestes plantilles des del propi fitxer
#	LDIF, és important no fer això ja que no sortirà efecte, ja que agafa aquestes plantilles
#	i li aplicarà les dades agafades de l'arxiu de configuració i crearà un nou fitxer LDIF
#	temporal, aquest fitxer temporal el modificarà i li aplicarà les dades pertinents.

# El punt més fort d'aquest procediment és que podem definir altres dades a afegir als arxius
#	LDIF, l'script només modifica les dades variables, és a dir, ID's, domini, grups, altres
#	dades les deixa igual. La modificació és mitjançant sed's.

# També tindre en compte les ID's, l'arxiu de configuració agafa la primera ID la qual
#	comença a comptar, pero no és un comptador de l'script realment, és la primera la qual
#	comença a comptar per agafar la primera lliure, és a dir, si creem una llista d'usuaris
#	i després esborrem un que estigui pel mitj tindrem un petit forat allà, però el próxim
#	usuari creat amb aquest script se li assignarà aquest forat, la primera ID lliure des del
#	primer nombre a comptar, la primera ID especificada a l'arxiu de configuració. El mateix s'aplica
#	amb els grups.

# Per la gestió d'usuaris utilitzarem els següents paràmetres:

#	# ./script.sh au nomusuari nomgrup	<- Crea un usuari en el grup especificat.
#	# ./script.sh ag nomgrup			<- Crea un grup.
#	# ./script.sh du nomusuari			<- Esborra un usuari.
#	# ./script.sh dg nomgrup			<- Esborra un grup.

# En cas d'quivocació de qualsevol paràmetre o paràmetre amb un erroni argument l'script
#	es detindrà i mostrarà un error.

# Per comoditat he deixat els missatges que retorna l'LDAP al fer la gestió d'usuaris i
#	grups per tal de poguer identificar el problema en cas de que hi hagi error al fer
#	l'operació, en cas de qualsevol error l'script també t'informa si ha pogut crear o
#	esborrar l'usuari o el grup.

# Com a últim detall, tenim la secció INFOCODE on podrem modificar absolutament tots els
#	missatges que surten per pantalla amb la fi de poguer traduir l'script a l'idioma
#	que vulguem nosaltres.

#----------------------------------------------------



#----------------------------------------------------
#		Explicació per a programadors
#----------------------------------------------------

# L'estructura efectiva d'aquest script (sense comptar els comentaris d'explicació) és:

#	- SOFTCODE 	<- Lloc on es depositen les variables de configuració primària de l'script.
#	- INFOCODE	<- Tots els missatges que surten amb l'execució de l'script.
#	- LIBCODE	<- En aquesta secció es defineixen les llibreríes que agafa aquest script.
#	- FUNCCODE	<- Secció on es depositen totes les funcions utilitzades a l'script.
#	- HARDCODE	<- Codi base de l'script, programa main, el core de l'script.
#	- ERRORCODE	<- Els missatges d'error que retorna l'script amb variables que referencien el INFOCODE.

#  ----SOFTCODE----
#	En el soft code d'aquest script hi trobem tots els valors configurables per a la posterior execució
#	de l'script, bàsicament aquí es troba la ruta de configuració i totes les altres configuracions
#	que aniràn dins del arxiu de configuració esmentades a l'explicació per a administrador de sistema.
#
#	Com bé diu a la secció, només s'aplicarán aquestes configuracions a l'hora de, o bé instal·larlo,
#	o bé a l'hora de resetejar les configuracions.

#  ----LIBCODE----
#	En aquesta secció només hi trobarem un fitxer, que es el necessari per agafar les variables 
#	que són a l'arxiu de configuració, si, realment el que configura l'usuari són més variables
#	quasi idéntiques a les del softcode, aquest script agafa aquest arxiu com a llibreria i
#	inclou aquestes variables com part de l'script.

#  ----FUNCCODE----
#	Tenim totes les funcions que es fan servir a l'script, les funcions que veiem aquí són en aquesta
#	secció perquè, o bé s'executen més d'una vegada i són aquí per reduïr codi, o bé perquè ocupen
#	una bona part del codi que sabem que fa només al fer una ullada.

#  ----HARDCODE----
#	Aquí tenim el core del programa, aquesta secció està dividida en subseccions que he anomenat móduls.
#	Aquests móduls són de fàcil gestió, i en cas d'haver-hi un error, la variable que indica el codi
#	d'error és substituïda per aquest, i ja no passa per cap módul més anant-se directament al ERROCODE.
#
#	Aquest sistema permet de forma sencilla la implementació de noves característiques al programa, com
#	poden ser noves comprovacions, noves adicions LDAP, nous arguments per fer més modificacions a la
#	configuració del programa, etc.
#
#	Si el programa ha fet amb éxit la seva funció fa la sortida del programa amb el seu corresponent missatge
#	informatiu de que ha fet l'operació amb èxit.
#
#	Sabent-hi que si hi ha qualsevol error modifica la variable d'error i si ha fet amb èxit les
#	operacions, si arriba al final del HARDCODE i la variable "ERRNUM" està a 0 significa que el
#	paràmetre que s'ha passat és incorrecte.

#  ----ERRORCODE----
#	Segons el número d'error a la variable ERRNUM mostrarà un error o un altre ubicat al INFOCODE i sortirà
#	del programa ja que al mostrar-lo no hi ha més codi per on ficar's-hi.

# Coses a tenir en compte si volem afegir nous mòduls a l'script:
#	1 - Entrar sempre amb un "if errnum = 0", amb això sabem que no hi ha hagut cap error posteriorment.
#	2 - Si finalitza sense cap error el mòdul, crear un missatge de finalització ubicat al infocode i
#		finalitzar l'execució de l'script.
#	3 - Si ha un error, indicar un nou codi d'error amb la variable "errnum", anar al ERRORCODE i crear
#		una nova entrada amb el codi d'error, finalment ubicar-lo al INFOCODE el missatge d'error.
#	4 - Ubicar el mòdul abans del mòdul 99 (dóna un salt de número per permetre la inseció de nous identificadors.)
#		que és el que detecta que l'argument és erroni per donar l'error.
#	5 - No és recomanable canviar de lloc els mòduls ja existents, ja que un mòdul sol suplir a un altre,
#		encara que sigui una programació basada en mòduls, alguns d'ells són indispensables ja que són els
#		que fan la funció principal de l'script.
#	6 - És recomanable que si repeteixes el mateix fragment de codi ho ubiquis al FUNCODE i el cridis
#		des d'allà.

#----------------------------------------------------



#----------------------------------------------------
#		Limitacions de l'script.
#----------------------------------------------------
#	1 - Domini només pot contidre subdomini.domini i no pot tenir espais.
#	2 - No està implementat el suport amb contrasenyes d'usuari.
#	3 - No comprova que sigui instal·lat l'LDAP al servidor remot.
#	4 - No pot fer servir més d'un servidor LDAP a l'hora.
#	5 - No comprova que estigui disponible el servidor LDAP al fer operacions.
#	6 - Si hi han més de 10000 grups o usuaris l'script no assigna més ID's i pot donar lloc a errors.
#	7 - No hi ha control de caràcters especials a la comprovació de domini.
#	8 - No comprova si s'introdueïxen espais en la creació d'usuaris i grups.
#	9 - No suporta multi-unitats organitzatives per a grups i usuaris.
#----------------------------------------------------



# -----------------------------------------
# -------------- SOFTCODE -----------------
# -----------------------------------------

#Ruta on aniràn a parar els fitxers
RUTA_CONF="."
#Ruta on anirà a parar el fitxer de configuració principal
NOM_FITX_CONF="ldif-script.conf"
#IMPORTANT - RESPECTAR DOBLES COMETES


#-------------------------------------------------------------------
#ATENCIÓ: SI JA HI HA UN ARXIU DE CONFIGURACIÓ CREAT, AQUESTES LÍNIES
#		  SÓN INÚTILS A NO SER QUE ES DECIDEIXI RECONFIGURAR CAP PARÀMETRE
#		  EN AQUEST CAS SI ES FARÀ SERVIR AQUESTS VALORS PER DEFECTE AL
#		  DEIXAR ELS CAMPS EN BLANC A L'ASSISTENT DE CONFIGURACIÓ.
#-------------------------------------------------------------------
#Ruta on anirà a parar el fitxer de contrasenya de admin ldap
NOM_FITX_PASSWD_PROV="ldif.passwd"

#Primer nombre a assignar IDs a grups.
FIRST_GID_PROV="10000"
#Primer nombre a assignar IDs a usuaris.
FIRST_UID_PROV="10000"

#Unitat organitzativa per a grups.
GROUP_SUFFIX_PROV="Groups"
#Unitat organitzativa per a users.
USER_SUFFIX_PROV="People"

#Domini i subdomini. Es important que sigui domini i subdomini, la modificació
#	d'aquest format pot donar lloc a errors.
DC_PROV="dam.ausias"

#IP del servidor LDAP.
IP_DOM_PROV="ldap://localhost"

#Nom d'usuari de l'administrador.
DC_ADMIN_PROV="admin"

#Password per defecte.
DC_PASS_PROV="blanc"

#Arxiu LDIF creació usuari.
ARX_LDIF_AU_PROV="plantilla_au.ldif"
#Arxiu LDIF creació grup.
ARX_LDIF_AG_PROV="plantilla_ag.ldif"
#Arxiu LDIF eliminació usuari.
ARX_LDIF_DU_PROV="plantilla_du.ldif"
#ARXIU LDIF eliminació grup.
ARX_LDIF_DG_PROV="plantilla_dg.ldif"
#-------------------------------------------------------------------


# -----------------------------------------
# -------------- INFOCODE -----------------
# -----------------------------------------

#Missatges de la creació de carpeta de configuració si no existeix.
MissMakeCar_1="Vols crear la carpeta de configuració? [S / N] "
MissMakeCarErr_1="Error, introdueïx una opció correcta."

#Missatges de l'assistent de configuració.
MissInst_1="No es troba el fitxer de configuració, procedint a crear-ne un de nou."
MissInst_2="Introdueïx un nom de domini (si el volem per defecte deixar en blanc):"
MissInst_3="Introdueïx una IP (si la volem per defecte deixar en blanc) format: ldap://localhost:"
MissInst_4="Aplicant IP assignada per defecte:"
MissInst_5="Introdueïx el nom d'usuari administrador (si la volem per defecte deixar en blanc):"
MissInst_6="Aplicant domini per defecte:"
MissInst_7="Aplicant nom d'usuari per defecte:"
MissInst_8="Introdueïx una contrasenya per l'administrador del domini."
MissInst_9="Contrasenya: "

#Si s'escull el mode automàtic.
MissInstNo_1="Mode no configuració escollit, aplicant valors per defecte."

#En cas de que el domini introduït no sigui vàlid.
MissErrInst_1="Error, aquest domini no és vàlid."

#Si no existeix el fitxer password.
MissErrPaswd_1="Error, no existeix el fitxer password, procedint a crear-lo."

#Línies que s'introdueïxen al fitxer de configuració.
MissInstConf_1="Primera ID de grup:"
MissInstConf_2="Primera ID d'usuari."
MissInstConf_3="Unitat organitzativa on s'agrupen els grups."
MissInstConf_4="Unitat organitzativa on s'agrupen els usuaris."
MissInstConf_5="Domini."
MissInstConf_6="SubDomini."
MissInstConf_7="IP del servidor LDAP."
MissInstConf_8="Nom d'usuari d'administrador del servidor LDAP."
MissInstConf_9="Nom de l'arxiu de la contrasenya d'admin LDAP."
MissInstConf_10="Nom de l'arxiu de la plantilla LDIF de creació d'usuari."
MissInstConf_11="Nom de l'arxiu de la plantilla LDIF de creació de grup."
MissInstConf_12="Nom de l'arxiu de la plantilla LDIF d'esborrat d'usuari."
MissInstConf_13="Nom de l'arxiu de la plantilla LDIF d'esborrat de grup."

#Missatges si no existeix qualsevol arxiu de plantilla LDIF.
MissErrFitx_1="No s'ha detectat la plantilla LDIF de creació d'usuaris, procedint a crear-la."
MissErrFitx_2="No s'ha detectat la plantilla LDIF de creació de grups, procedint a crear-la."
MissErrFitx_3="No s'ha detectat la plantilla LDIF d'esborrat d'usuaris, procedint a crear-la."
MissErrFitx_4="No s'ha detectat la plantilla LDIF d'esborrat de grups, procedint a crear-la."

#Missatges d'error de final de programa.
MissErr_1="Error[1] -> No s'ha executat aquest script com a root."
MissErr_2="Error[2] -> Introdueïx un paràmetre al programa."
MissErr_3="Error[3] -> No es pot determinar el domini assignat per defecte."
MissErr_4="Error[4] -> Ja existeix l'arxiu de configuració, si vols borrar-ho executa l'script en mode desinstal·lació."
MissErr_5="Error[5] -> No s'han eliminat els següents fitxers perque no existeixen:"
MissErr_6="Error[6] -> Error, el programa no està instal·lat."
MissErr_7="Error[7] -> Paquet indispensable no instal·lat:"
MissErr_8="Error[8] -> No s'ha creat la carpeta on depositar els fitxers."
MissErr_9="Error[9] -> Falta algun paràmetre."
MissErr_10="Error[10] -> No existeix el grup introduït."
MissErr_11="Error[11] -> Ja existeix l'usuari introduït."
MissErr_12="Error[12] -> Ja existeix el grup introduït."
MissErr_13="Error[13] -> No existeix l'usuari introduït."
MissErr_14="Error[14] -> No s'ha pogut esborrar el grup."
MissErr_15="Error[15] -> No s'ha pogut esborrar l'usuari."
MissErr_16="Error[16] -> No s'ha pogut crear el grup."
MissErr_17="Error[17] -> No s'ha pogut crear l'usuari."
MissErr_18="Error[18] -> No s'ha pogut canviar la IP del servidor LDAP."
MissErr_19="Error[19] -> No s'ha pogut canviar el domini."
MissErr_20="Error[20] -> Introdueïx un paràmetre correcte."

#Missatges de finalització del programa sense errors.
MissExit_1="S'ha eliminat el grup amb èxit."
MissExit_2="S'ha eliminat l'usuari amb èxit."
MissExit_3="S'ha creat el grup amb èxit."
MissExit_4="S'ha creat l'usuari amb èxit."
MissExit_5="Operació d'esborrat completada amb éxit."
MissExit_6="Contrasenya d'admin LDAP canviada."
MissExit_7="IP del servidor LDAP canviada."
MissExit_8="S'ha canviat el domini exitósament."

# -----------------------------------------
# -------------- LIBCODE ------------------
# -----------------------------------------

#Si no existeix el fitxer de configuració no l'incloguis ja que el crea més endavant i defineix les variables corresponents.
if [ -e "$RUTA_CONF"/"$NOM_FITX_CONF" ]
then
	source "$RUTA_CONF"/"$NOM_FITX_CONF"
fi

# -----------------------------------------
# -------------- FUNCCODE -----------------
# -----------------------------------------

#Obtenció del primer ID disponible a partir del primer especificat a l'arxiu de configuració.
function gidnumb () {
	#Recorre 10000 usuaris en busca de la ID.
	for e in {1..10000}
	do
		#Suma-li al comptador del for la primera ID especificada i resta-li 1 per començar des de 0.
		local GIDNUM=`expr $e + $F_GID - 1`
		#Fem una çerca per agafar la id corresponent a la línia ordenada del comptador e i la comparem amb la ID que hem adquirit a partir
		#	de la suma anterior depositada a la variable definida amb GIDNUM, si no correspon significa que hi ha una ID disponible que no
		#	està assignada a cap grup, un breu exemple:
		#	ID obtinguda	ID corresponent
		#		10000			10000
		#		10001			10001
		#		10002			10002
		#		10004			10003	<- Veiem com és diferent, per tant aquí faria el break i retornaria aquesta ID per ser assignada al grup creat.
		if [[ `ldapsearch -x -b ou=$G_SUFFIX,dc=$DC_SE,dc=$DC_PR -H $IP_DOM | grep gidNumber | cut -d ' ' -f 2 | sort | head -n $e | tail -n 1` != $GIDNUM ]]
		then
			echo $GIDNUM
			break
		fi
	done
}

#Mateix funcionament que la funció gidnumb sol que actúa amb ID's d'usuari.
function uidnumb () {
	for e in {1..10000}
	do
		local UIDNUM=`expr $e + $F_UID - 1`
		if [[ `ldapsearch -x -b ou=$U_SUFFIX,dc=$DC_SE,dc=$DC_PR -H $IP_DOM | grep uidNumber | cut -d ' ' -f 2 | sort | head -n $e | tail -n 1` != $UIDNUM ]]
		then
			echo $UIDNUM
			#AUX és el següent valor a assignar
			#return $AUX
			break
		fi
	done
}

#Comprova que el domini sigui en format: subdomini.domini
function validate_domini () {
	#Declarem el paràmetre passat com a primer argument a la funció.
	local COMPL="$1"
	#Analitzem si abans del primer espai el domini segueix sent el mateix, això detecta si l'argument passat
	#	com domini té espais, si no té serà igual.
	if [[ `echo "$COMPL" | cut -d ' ' -f 1` != "$COMPL" ]]
	then
		return 1
	#Analitzem si hi ha un segon punt al domini passat com a argument, si no hi ha no mostrarà res i la comprovació
	#	serà negativa, en cas contrari retornarà 1 que indica que si hi ha hagut error.
	elif [[ `echo "$COMPL" | cut -d '.' -f 3` != '' ]]
	then
		return 1
	fi
	#Si supera aquestes dues comprovacions vol dir que el domini és correcte.
	return 0
}

#Com aquesta funció es repeteix dues vegades la he ficat com a tal, aquesta assigna un nom de domini a les dues
#	variables de domini i subdomini i fa les comprovacions pertinents.
function assignar_nom_domini () {

	AUX=1
	#Cicle infinit mentres l'usuari no assigni un nom de domini correcte.
	while (( $AUX == 1 ))
	do
		#Li demanem un domini a l'usuari.
		read -p "$MissInst_2 -> " DC_COMPL_AUX #Introdueïx un nom de domini (si el volem per defecte deixar en blanc):
		#Si no ho deixem en blanc procedim a comprovar el domini introduït.
		if ! [[ "$DC_COMPL_AUX" == '' ]] && ! [[ "$DC_COMPL_AUX" == ' ' ]]
		then
			validate_domini "$DC_COMPL_AUX"
		fi
		#Si el retornament de la funció no és 1 (error) procedirà a configurar els valors com a domini sel·leccionat.
		if [ $? -eq 0 ]
		then
			#En cas d'haver deixat en blanc la opció procedirà a agafar el que està definit per defecte al SOFTCODE.
			if [[ "$DC_COMPL_AUX" == '' ]] || [[ "$DC_COMPL_AUX" == ' ' ]]
			then
				DC_COMPL_AUX="$DC_PROV"
				echo "$MissInst_6 $DC_PROV" #Aplicant domini per defecte
			fi
			#Defineix com a subdomini el que hi ha darrera del primer punt i com a domini el que hi ha després del primer punt.
			DC_PR=`echo "$DC_COMPL_AUX" | cut -d '.' -f 2`
			DC_SE=`echo "$DC_COMPL_AUX" | cut -d '.' -f 1`
			break
		fi
		echo "$MissErrInst_1" #Error, aquest domini no és vàlid.
	done

}

#Demana una IP a l'usuari i l'assigna a la variable.
function assignar_ip () {

	read -p "$MissInst_3 -> " IP_DOM_AUX #Introdueïx una IP (si la volem per defecte deixar en blanc) format: lpad://localhost:
	#Si no es espai en blanc la introducció per l'usuari defineix-la a la variable de IP de domini.
	if ! [[ "$IP_DOM_AUX" == '' ]] && ! [[ "$IP_DCOM_AUX" == ' ' ]]
	then
		IP_DOM="$IP_DOM_AUX"
	#En cas de que l'usuari no hagi introduït cap IP assignarà la que ja hi ha per defecte.
	else
		IP_DOM="$IP_DOM_PROV"
		echo "$MissInst_4 $IP_DOM" #Aplicant IP assignada per defecte:.
	fi
}

#Exactament igual que amb la funció d'assignar IP però aquesta vegada amb l'usuari d'administrador.
function assignar_administrador () {

	read -p "$MissInst_5 -> " DC_ADMIN_AUX #Introdueïx el nom d'usuari administrador (si la volem per defecte deixar en blanc):
	if ! [[ "$DC_ADMIN_AUX" == '' ]] && ! [[ "$DC_ADMIN_AUX" == ' ' ]]
	then
		DC_ADMIN="$DC_ADMIN_AUX"
	else
		DC_ADMIN="$DC_ADMIN_PROV"
		echo "$MissInst_7 $DC_ADMIN" #Aplicant nom d'usuari per defecte:
	fi
}

#Crea el fitxer amb la contrasenya d'administrador del LDAP i li assigna els permissos pertinents.
function assignar_pass_ldap () {

	echo "$MissInst_8" #Introdueïx una contrasenya per l'administrador del domini.
	#Demana contrasenya amb el camp d'introducció invisible (paràmetre -s).
	read -sp "$MissInst_9" DC_PASS #Contrasenya: 
	#Afegeix la contrasenya a l'arxiu de password
	echo "$DC_PASS" > "$RUTA_CONF"/"$NOM_FITX_PASSWD"
	#Li assigna permissos de només lectura.
	chmod 400 "$RUTA_CONF"/"$NOM_FITX_PASSWD"
	echo " "
}

function comprovar_group () {

	#Fa una çerca en busca del grup especificat com a primer argument d'aquesta crida.
	ldapsearch -x -b cn="$1",ou="$G_SUFFIX",dc="$DC_SE",dc="$DC_PR" -H "$IP_DOM" &> /dev/null
	#Si retorna cap error significa que si existeix el grup, i li retornarem 1 com que ja existeix.
	if [ $? -eq 0 ]
	then 
		return 1
	fi
	#Si no existeix la anterior çerca retornarà 1 com error de que no existeix, nosaltres li retornarem
	#	0 com que no existeix.
	return 0
	#comprovar_grup... Existeix el grup? 1 - Si, Existeix el grup? 0 - No.
}

#El mateix que l'anterior funció de comprovar grup.
function comprovar_user () {

	ldapsearch -x -b uid="$1",ou="$U_SUFFIX",dc="$DC_SE",dc="$DC_PR" -H "$IP_DOM" &> /dev/null
	if [ $? -eq 0 ]
	then
		return 1
	fi
	return 0
}

#Creació de l'arxiu LDIF per crear usuaris.
function make_ldif_au () {

	touch "$RUTA_CONF"/"$ARX_LDIF_AU"
	echo "dn: uid=exempleldif,ou=$U_SUFFIX,dc=$DC_SE,dc=$DC_PR" >> "$RUTA_CONF"/"$ARX_LDIF_AU"
	echo "changetype: add" >> "$RUTA_CONF"/"$ARX_LDIF_AU"
	echo "cn: exempleldif" >> "$RUTA_CONF"/"$ARX_LDIF_AU"
	echo "description: Compte creat amb ldif scripts." >> "$RUTA_CONF"/"$ARX_LDIF_AU"
	echo "gecos: exempleldif" >> "$RUTA_CONF"/"$ARX_LDIF_AU"
	echo "gidnumber: $F_GID" >> "$RUTA_CONF"/"$ARX_LDIF_AU"
	echo "homedirectory: /home/exempleldif" >> "$RUTA_CONF"/"$ARX_LDIF_AU"
	echo "loginshell: /bin/bash" >> "$RUTA_CONF"/"$ARX_LDIF_AU"
	echo "objectclass: account" >> "$RUTA_CONF"/"$ARX_LDIF_AU"
	echo "objectclass: posixAccount" >> "$RUTA_CONF"/"$ARX_LDIF_AU"
	echo "uid: exempleldif" >> "$RUTA_CONF"/"$ARX_LDIF_AU"
	echo "uidnumber: $F_UID" >> "$RUTA_CONF"/"$ARX_LDIF_AU"
}

#Creació de l'arxiu LDIF per crear grups.
function make_ldif_ag () {

	touch "$RUTA_CONF"/"$ARX_LDIF_AG"
	echo "dn: cn=gexempleldif,ou=$G_SUFFIX,dc=$DC_SE,dc=$DC_PR" >> "$RUTA_CONF"/"$ARX_LDIF_AG"
	echo "cn: gexempleldif" >> "$RUTA_CONF"/"$ARX_LDIF_AG"
	echo "description: Grup creat amb ldif scripts." >> "$RUTA_CONF"/"$ARX_LDIF_AG"
	echo "gidnumber: $F_GID" >> "$RUTA_CONF"/"$ARX_LDIF_AG"
	echo "objectclass: posixGroup" >> "$RUTA_CONF"/"$ARX_LDIF_AG"
}

#Creació de l'arxiu LDIF per esborrar usuaris.
function make_ldif_du () {

	touch "$RUTA_CONF"/"$ARX_LDIF_DU"
	echo "dn: cn=exempleldif,ou=$U_SUFFIX,dc=$DC_SE,dc=$DC_PR" >> "$RUTA_CONF"/"$ARX_LDIF_DU"
	echo "changetype: delete" >> "$RUTA_CONF"/"$ARX_LDIF_DU"
}

#creació de l'arxiu LDIF per esborrar grups.
function make_ldif_dg () {

	touch "$RUTA_CONF"/"$ARX_LDIF_DG"
	echo "dn: cn=gexempleldif,ou=$G_SUFFIX,dc=$DC_SE,dc=$DC_PR" >> "$RUTA_CONF"/"$ARX_LDIF_DG"
	echo "changetype: delete" >> "$RUTA_CONF"/"$ARX_LDIF_DG"
}

#Aquesta funció va ser creada amb l'objectiu de no repetir aquest fragment de codi
#	el número de vegades corresponent als arxius creats per l'script, li passem
#	com a argument l'arxiu, per comprendre millor aquesta funció mirar el mòdul 05.
function del_program () {

	if [ -e "$RUTA_CONF"/"$1" ] && ! [ -d "$RUTA_CONF"/"$1" ]
	then
		#Si existeix el fixter i no és un directori l'esborrarem.
		rm "$RUTA_CONF"/"$1"
	else
		#Si no existeix el fitxer declararem l'error, i afegirem el nom del fitxer
		#	a l'arxiu on mostrarem al final al mostrar l'error.
		ERRNUM=5
		let "AUX += 1"
		echo "$1" >> "$ARX_RM_TMP"
	fi
}

# -----------------------------------------
# -------------- HARDCODE -----------------
# -----------------------------------------
#Inicialitzem la variable d'error a 0
ERRNUM=0
#Inicialitzem la variable de comprovació de si s'ha configurat l'arxiu de configuració.
CHECK_CONF=0
#Inicialitzem la variable de comprovació de si s'ha creat l'arxiu de password.
CHECK_PASSWD=0
#Nom de l'arxiu temporal on s'emmagatzemarà la plantilla LDIF que s'aplicarà en l'instant de fer qualsevol operació al LDAP.
ARX_LDIF_TMP="tmp.ldif"
#Nom de l'arxiu temporal on s'emmagatzemaràn els nom de fitxers que no s'han eliminat en l'operació d'esborrat.
ARX_RM_TMP="rm_tmp.txt"


echo "Adrian Martínez Montes"

#------------------------------------------------------------------------
#--- Comprovació si és root
#---  MÒDUL 00
#------------------------------------------------------------------------
if (( `id -u` != 0 ))
then
	ERRNUM=1
fi
#------------------------------------------------------------------------


#------------------------------------------------------------------------
#--- Comprovació carpeta de configuració
#---  MÒDUL 01
#------------------------------------------------------------------------
if (( $ERRNUM == 0 ))
then
	#Si no existeix la carpeta de configuració i no hem escollit la opció de desinstal·lar el programa.
	if ! [ -d "$RUTA_CONF" ] && ! [[ "$1" == 'rm' ]]
	then
		#Si hem escollit l'opció d'instal·lació automàtica posarem aquesta variable con a S
		if [[ "$1" == 'noIn' ]]
		then
			OPC=S
		#En cas contrari li demanarem a l'usuari si vol crear la carpeta.
		else
			read -p "$MissMakeCar_1 " OPC #Vols crear la carpeta de configuració? [S / N]
		fi
		while [[ "$OPC" != "S" ]] && [[ "$OPC" != "N" ]]
		do
			echo "$MissMakeCarErr_1" #Error, introdueïx una opció correcta.
			read -p "$MissMakeCar_1 " OPC #Vols crear la carpeta de configuració?
		done
		#Si la opció ha sigut S llavors crearem la carpeta.
		if [[ "$OPC" == "S" ]]
		then
			mkdir "$RUTA_CONF"
			#Si ha hagut qualsevol error en la creació de la carpeta mostrarem error per pantalla.
			if ! [ $? -eq 0 ]
			then
				ERRNUM=8
			fi
		#En cas contrari, si la opció ha sigut N mostrarem el missatge d'error.
		else
			ERRNUM=8
		fi
	fi
fi
#------------------------------------------------------------------------


#------------------------------------------------------------------------
#--- Comprovació si estàn instal·lats els programes necessaris
#--- Innecesari si es vol accedir mediant remot al LDAP
#---  MÒDUL 02
#------------------------------------------------------------------------
#if (( $ERRNUM == 0 ))
#then
#	if [[ `dpkg --get-selections ldap-utils | cut -f 1` != "ldap-utils" ]]
#	then
#		ERRNUM=7
#		DPKG="ldap-utils"
#	else
#		if [[ `dpkg --get-selections slapd | cut -f 1` != "slapd" ]]
#		then
#			ERRNUM=7
#			DPKG="slapd"
#		fi
#	fi
#fi
#------------------------------------------------------------------------


#------------------------------------------------------------------------
#--- Comprovació si s'han especificat arguments
#---  MÒDUL 03
#------------------------------------------------------------------------
if (( $ERRNUM == 0 ))
then
	#Si existeix el fitxer de configuració i no s'han introduït arguments donarà el missatge d'error.
	if [[ $1 == '' ]] && [ -e "$RUTA_CONF"/"$NOM_FITX_CONF" ]
	then
		ERRNUM=2
	fi
fi
#------------------------------------------------------------------------



#------------------------------------------------------------------------
#--- Comprovació si el domini especificat per defecte és correcte
#---  MÒDUL 04
#------------------------------------------------------------------------
#Aquest mòdul comprova que el domini possat com a variable al softcode és correcte.
if (( $ERRNUM == 0 ))
then
	#Només ho comprova si no existeix al fitxer de configuracióo si s'especifica que volem
	#	fer el reset de domini, ja que en els dos casos s'utilitza aquesta variable.
	if ! [ -e "$RUTA_CONF"/"$NOM_FITX_CONF" ] || [[ "$1" == "resD" ]]
	then
		#Validem el domini...
		validate_domini "$DC_PROV"
		#Si torna 1 es que no és vàlid aquest domini.
		if [ $? -eq 1 ]
		then
			ERRNUM=3
		fi
	fi
fi
#------------------------------------------------------------------------



#------------------------------------------------------------------------
#--- Desinstal·lació del programa
#---  MÒDUL 05
#------------------------------------------------------------------------
if (( $ERRNUM == 0 ))
then
	if [[ "$1" == "rm" ]]
	then
		#Si existeix el directori on s'allotja els fitxers de configuració, del contrari
		#	mostrarà un missatge d'error.
		if [ -d "$RUTA_CONF" ]
		then
			#Creem l'arxiu temporal on depositarem el nom dels arxius que no s'han pogut esborrar.
			touch "$ARX_RM_TMP"
			#Inicialitzem la variable AUX a 0
			AUX=0
			#Esborrem els fitxers que composen la configuració de l'script.
			del_program "$NOM_FITX_CONF"
			del_program "$NOM_FITX_PASSWD"
			del_program "$ARX_LDIF_AU"
			del_program "$ARX_LDIF_AG"
			del_program "$ARX_LDIF_DU"
			del_program "$ARX_LDIF_DG"
			#Si la carpeta queda buida l'esborrarem.
			if ! [ "$(ls -A $RUTA_CONF)" ]
			then
				rm -R "$RUTA_CONF"
			fi
		else
			ERRNUM=6
			AUX=0
		fi

		#Si no s'ha definit un codi d'error vol dir que s'han esborrat tots els arxius.
		if (( $ERRNUM == 0 ))
		then
			rm "$ARX_RM_TMP"
			echo "$MissExit_5" #Operació d'esborrat completada amb éxit.
			exit 0
		#Si la variable AUX ha arribat a 6 (es suma +1 per cada arxiu amb un màxim de 6 arxius)
		#	vol dir que s'han esborrat tots els arxius amb èxit.
		elif (( $AUX == 6 ))
		then
			#Definirem el codi d'error corresponent.
			ERRNUM=6
			#Esborrem l'arxiu temporal on s'havien de depositar els nom dels arxius que no s'esborrarien.
			rm "$ARX_RM_TMP"
		fi
	fi
fi
#------------------------------------------------------------------------



#------------------------------------------------------------------------
#--- Procés d'instal·lació arxiu de configuració
#---  MÒDUL 06
#------------------------------------------------------------------------
if (( $ERRNUM == 0 ))
then
	#Si no existeix l'arxiu de configuració o s'ha especificat l'argument -resA procedirem a crear
	#	l'arxiu de configuració.
	if ! [ -e "$RUTA_CONF"/"$NOM_FITX_CONF" ] || [[ "$1" == "resA" ]] || [[ "$2" == "resA" ]]
	then
		#Definirem les variables reals amb les provisional que les farem utilitzar més endavant.
		F_GID="$FIRST_GID_PROV"
		F_UID="$FIRST_UID_PROV"
		G_SUFFIX="$GROUP_SUFFIX_PROV"
		U_SUFFIX="$USER_SUFFIX_PROV"
		#Retallem domini primari de la variable de domini.
		DC_PR=`echo "$DC_PROV" | cut -d '.' -f 2`
		#Retallem domini secundari de la variable de domini.
		DC_SE=`echo "$DC_PROV" | cut -d '.' -f 1`
		IP_DOM="$IP_DOM_PROV"
		DC_ADMIN="$DC_ADMIN_PROV"
		DC_PASS="$DC_PASS_PROV"
		ARX_LDIF_AU="$ARX_LDIF_AU_PROV"
		ARX_LDIF_AG="$ARX_LDIF_AG_PROV"
		ARX_LDIF_DU="$ARX_LDIF_DU_PROV"
		ARX_LDIF_DG="$ARX_LDIF_DG_PROV"
		NOM_FITX_PASSWD="$NOM_FITX_PASSWD_PROV"
		#Si s'ha especificat l'argument resA esborrarem l'arxiu de configuració.
		if [[ "$1" == "resA" ]] || [[ "$2" == "resA" ]]
		then
			rm "$RUTA_CONF"/"$NOM_FITX_CONF"
		else
			#En cas contrari informarem a l'usuari de que no es troba el fitxer de configuració.
			echo "$MissInst_1" #No es troba el fitxer de configuració, procedint a crear-ne un de nou.
		fi
		#Si s'ha escollit el mode automàtic informarem a l'usuari i crearem directament el fitxer de password
		#	amb el valor de contrasenya per defecte.
		if [[ "$1" == "noIn" ]] || [[ "$2" == "noIn" ]]
		then
			echo "$MissInstNo_1" #Mode no configuració escollit, aplicant valors per defecte.
			echo "$DC_PASS" > "$RUTA_CONF"/"$NOM_FITX_PASSWD"
			chmod 400 "$RUTA_CONF"/"$NOM_FITX_PASSWD"
		else
			#En cas contrari demanarem les dades corresponents a l'usuari. En cas de deixar en blanc les opcions demanades
			#	a l'usuari s'aplicaràn els valors per defecte al sofcode.
			assignar_nom_domini
			assignar_ip
			assignar_administrador
			assignar_pass_ldap
		fi

		#I aquí es crea l'arxiu de configuració, es crea una línia informant de que significa cada variable i la següent
		#	la variable en qüestió.
		echo "#$MissInstConf_1" > "$RUTA_CONF"/"$NOM_FITX_CONF" #Primera ID de grup:
		echo "F_GID=\"$F_GID\"" >> "$RUTA_CONF"/"$NOM_FITX_CONF"

		echo "#$MissInstConf_2" >> "$RUTA_CONF"/"$NOM_FITX_CONF" #Primera ID d'usuari.
		echo "F_UID=\"$F_UID\"" >> "$RUTA_CONF"/"$NOM_FITX_CONF"

		echo "#$MissInstConf_3" >> "$RUTA_CONF"/"$NOM_FITX_CONF" #Unitat organitzativa on s'agrupen els grups.
		echo "G_SUFFIX=\"$G_SUFFIX\"" >> "$RUTA_CONF"/"$NOM_FITX_CONF"

		echo "#$MissInstConf_4" >> "$RUTA_CONF"/"$NOM_FITX_CONF" #Unitat organitzativa on s'agrupen els usuaris.
		echo "U_SUFFIX=\"$U_SUFFIX\"" >> "$RUTA_CONF"/"$NOM_FITX_CONF"

		echo "#$MissInstConf_5" >> "$RUTA_CONF"/"$NOM_FITX_CONF" #Domini.
		echo "DC_PR=\"$DC_PR\"" >> "$RUTA_CONF"/"$NOM_FITX_CONF"

		echo "#$MissInstConf_6" >> "$RUTA_CONF"/"$NOM_FITX_CONF" #SubDomini.
		echo "DC_SE=\"$DC_SE\"" >> "$RUTA_CONF"/"$NOM_FITX_CONF"

		echo "#$MissInstConf_7" >> "$RUTA_CONF"/"$NOM_FITX_CONF" #IP del servidor LDAP.
		echo "IP_DOM=\"$IP_DOM\"" >> "$RUTA_CONF"/"$NOM_FITX_CONF"

		echo "#$MissInstConf_8" >> "$RUTA_CONF"/"$NOM_FITX_CONF" #Nom d'usuari d'administrador del servidor LDAP.
		echo "DC_ADMIN=\"$DC_ADMIN\"" >> "$RUTA_CONF"/"$NOM_FITX_CONF"

		echo "#$MissInstConf_9" >> "$RUTA_CONF"/"$NOM_FITX_CONF" #Nom de l'arxiu de la contrasenya d'admin LDAP.
		echo "NOM_FITX_PASSWD=\"$NOM_FITX_PASSWD\"" >> "$RUTA_CONF"/"$NOM_FITX_CONF"

		echo "#$MissInstConf_10" >> "$RUTA_CONF"/"$NOM_FITX_CONF" #Nom de l'arxiu de la plantilla LDIF de creació d'usuari.
		echo "ARX_LDIF_AU=\"$ARX_LDIF_AU\"" >> "$RUTA_CONF"/"$NOM_FITX_CONF"

		echo "#$MissInstConf_11" >> "$RUTA_CONF"/"$NOM_FITX_CONF" #Nom de l'arxiu de la plantilla LDIF de creació de grup.
		echo "ARX_LDIF_AG=\"$ARX_LDIF_AG\"" >> "$RUTA_CONF"/"$NOM_FITX_CONF"

		echo "#$MissInstConf_12" >> "$RUTA_CONF"/"$NOM_FITX_CONF" #Nom de l'arxiu de la plantilla LDIF d'esborrat d'usuari.
		echo "ARX_LDIF_DU=\"$ARX_LDIF_DU\"" >> "$RUTA_CONF"/"$NOM_FITX_CONF"

		echo "#$MissInstConf_13" >> "$RUTA_CONF"/"$NOM_FITX_CONF" #Nom de l'arxiu de la plantilla LDIF d'esborrat de grup.
		echo "ARX_LDIF_DG=\"$ARX_LDIF_DG\"" >> "$RUTA_CONF"/"$NOM_FITX_CONF"

		#Definim la variable CHECK_CONF a 1 per fer-la servir després.
		CHECK_CONF=1

	#Si existeix l'arxiu de configuració i s'ha demanat una instal·lació automàtica mostrarà un error de que ja està instal·lat.
	elif [[ "$1" == "noIn" ]]
	then
		ERRNUM=4
	fi	
fi

#------------------------------------------------------------------------



#------------------------------------------------------------------------
#--- Creació fitxers LDAP
#---  MÒDUL 07
#------------------------------------------------------------------------
#Detecta si falta qualsevol arxiu de plantilla LDIF i el crea automàticament, seràn necessaris
#	més endavant per fer la gestió d'usuaris i grups.
if (( $ERRNUM == 0 ))
then
	#Si no existeix l'arxiu LDIF mostra un missatge per pantalla.
	if ! [ -e "$RUTA_CONF"/"$ARX_LDIF_AU" ]
	then
		echo "$MissErrFitx_1" #No s'ha detectat la plantilla LDIF de creació d'usuaris, procedint a crear-la.
		#Crida a la funció que crea el fitxer corresponent, en aquest cast la funció que crea l'arxiu LDIF de creació d'usuaris.
		make_ldif_au
	fi
	if ! [ -e "$RUTA_CONF"/"$ARX_LDIF_AG" ]
	then
		echo "$MissErrFitx_2" #No s'ha detectat la plantilla LDIF de creació de grups, procedint a crear-la.
		make_ldif_ag
	fi
	if ! [ -e "$RUTA_CONF"/"$ARX_LDIF_DU" ]
	then
		echo "$MissErrFitx_3" #No s'ha detectat la plantilla LDIF d'esborrat d'usuaris, procedint a crear-la.
		make_ldif_du
	fi
	if ! [ -e "$RUTA_CONF"/"$ARX_LDIF_DG" ]
	then
		echo "$MissErrFitx_4" #No s'ha detectat la plantilla LDIF d'esborrat de grups, procedint a crear-la.
		make_ldif_dg
	fi
fi
#------------------------------------------------------------------------



#------------------------------------------------------------------------
#--- Creació arxiu de password si no existeix
#---  MÒDUL 08
#------------------------------------------------------------------------
if (( $ERRNUM == 0 ))
then
	#Si es detecta que no existeix el fitxer password el crearà.
	if ! [ -e "$RUTA_CONF"/"$NOM_FITX_PASSWD" ]
	then
		echo "$MissErrPaswd_1" #Error, no existeix el fitxer password, procedint a crear-lo.
		#Cridem a la funció que demana contrasenya i crea el fitxer amb aquesta.
		assignar_pass_ldap
		#Definim la variable de que s'ha creat el fitxer password per fer ús després.
		CHECK_PASSWD=1
	fi
	#Definim la variable DC_PASS amb la password allotjada al fitxer password, sempre passarà
	#	per aquí ja que serà necessària posteriorment, no passarà per aquí si hi ha hagut qualsevol error.
	DC_PASS="`cat $RUTA_CONF/$NOM_FITX_PASSWD`"
fi
#------------------------------------------------------------------------



#------------------------------------------------------------------------
#--- Canviar nom de domini
#---  MÒDUL 09
#------------------------------------------------------------------------
#Si no s'ha fet la prèvia configuració de l'arxiu de configuració passa per aquí, del contrari
#	no fa falta ja que s'acaba de configurar.
if (( $ERRNUM == 0 )) && (( $CHECK_CONF == 0 ))
then
	#Si hem especificat resD com a primer paràmetre entrarà per aquí per fer el reset de domini.
	if [[ "$1" == "resD" ]]
	then
		#Si el segon paràmetre està buit demanarem un domini.
		if [[ "$2" == '' ]]
		then
			#Crida de la funció per especificar el domini.
			assignar_nom_domini
		else
			#Si s'ha especificat un domini con a segon argument es validarà de la mateixa forma que a la funció.
			AUX=1
			COMPR=0
			#Especifiquem el domini del argument a aquesta variable per fer més cómode el seu ús.
			DC_COMPL_AUX="$2"
			while (( $AUX == 1 ))
			do
				#Si al validar el domini dona error aquesta variable es tornarà 1 perquè arriba a final del while i tornarà
				#	a demanar un domini.
				if (( $COMPR == 1 ))
				then
					read -p "$MissInst_2 -> " DC_COMPL_AUX #Introdueïx un nom de domini (si el volem per defecte deixar en blanc):
				fi
				#Valida el domini emmagatzemat a la variable DC_COMPL_AUX
				if ! [[ "$DC_COMPL_AUX" == '' ]] && ! [[ "$DC_COMPL_AUX" == ' ' ]]
				then
					validate_domini "$DC_COMPL_AUX"
				fi
				#Si no dóna error o si s'ha deixat en blanc assignarem el domini ja emmagatzemat aquí.
				if [ $? -eq 0 ] || [[ "$DC_COMPL_AUX" == '' ]] || [[ "$DC_COMPL_AUX" == ' ' ]]
				then
					#Si d'ha deixat en blanc agafarem la variable provisional definida al softcode.
					if [[ "$DC_COMPL_AUX" == '' ]] || [[ "$DC_COMPL_AUX" == ' ' ]]
					then
						DC_COMPL_AUX="$DC_PROV"
					fi
					#Retallem el domini...
					DC_PR=`echo "$DC_COMPL_AUX" | cut -d '.' -f 2`
					DC_SE=`echo "$DC_COMPL_AUX" | cut -d '.' -f 1`
					break
				fi
				#Si arriba aquí es que el domini no ha sigut validat, per tant el bucle while seguirà funcionant.
				echo "$MissErrInst_1" #Error, aquest domini no és vàlid.
				#Definim aquesta variable a 1 per demanar posteriorment el domini.
				COMPR=1
			done
		fi
		#Modifiquem el fitxer de configuració amb el valor corresponent de domini.
		sed -i "s|^DC_PR.*|DC_PR=\"$DC_PR\"|" "$RUTA_CONF"/"$NOM_FITX_CONF"
		if [ $? -eq 0 ]
		then
			#Si no hi ha cap error en la modificació asignarem el subdomini a la línia corresponent.
			sed -i "s|^DC_SE.*|DC_SE=\"$DC_SE\"|" "$RUTA_CONF"/"$NOM_FITX_CONF"
			#NOTA: sed -i per insertar i signes d'adjunció per a insertar valors de variables.
			if [ $? -eq 0 ]
			then
				#Si no dóna error tampoc mostrarem missatge d'èxit.
				echo "$MissExit_8" #S'ha canviat el domini exitósament.
				exit 0
			else
				#Si hi ha error, assignem codi d'error.
				ERRNUM=19
			fi
		#Si hi ha error, assignem codi d'error.
		else
			ERRNUM=19
		fi
	fi
fi
#------------------------------------------------------------------------



#------------------------------------------------------------------------
#--- Canviar IP LDAP
#---  MÒDUL 10
#------------------------------------------------------------------------
#Mateixa condició que al fer el reset de domini al mòdul 09
if (( $ERRNUM == 0 )) && (( $CHECK_CONF == 0 ))
then
	#Si el primer argument és resI procedim a fer el reset de la IP.
	if [[ "$1" == "resI" ]]
	then
		#Si hem deixat el segon argument en blanc cridarem a la funció per demanar les dades de IP.
		if [[ "$2" == '' ]] || [[ "$2" == ' ' ]]
		then
			assignar_ip
		else
			#En cas contrari assignem l'argument a la variable de la IP
			IP_DOM="$2"
		fi
		#Modifiquem l'arxiu per insertar la línia corresponent amb les dades corresponents.
		sed -i "s|^IP_DOM.*|IP_DOM=\"$IP_DOM\"|" "$RUTA_CONF"/"$NOM_FITX_CONF"
		if [ $? -eq 0 ]
		then
			echo "$MissExit_7" #IP del servidor LDAP canviada.
			exit 0
		else
			#Si sorgeix error en la inserció assignem el codi d'error.
			ERRNUM=18
		fi
	fi

fi
#------------------------------------------------------------------------



#------------------------------------------------------------------------
#--- Canviar la pass d'admin LDAP
#---  MÒDUL 11
#------------------------------------------------------------------------
#Comprovem que no s'hagi iniciat l'assistent de configuració ni que s'hagi ja assignat
#	una contrasenya ja que no tindria sentit tornar a demanar-la.
if (( $ERRNUM == 0 )) && (( $CHECK_CONF == 0)) && (( $CHECK_PASSWD == 0 ))
then
	if [[ "$1" == "resP" ]]
	then
		#Cridem a la funció per l'assignació de la contrasenya.
		assignar_pass_ldap
		echo "$MissExit_6" #Contrasenya d'admin LDAP canviada.
		exit 0
	fi
fi
#------------------------------------------------------------------------



#------------------------------------------------------------------------
#--- Crear usuari i grup; eliminar usuari i grup
#---  MÒDUL 12
#------------------------------------------------------------------------
#Bé podria haver-hi separat aquest mòdul en 4 diferents, però no ho he fet ja que la
#	funció real que demanava la pràctica és tot aquest mòdul i no li veia sentit a 
#	separar-lo.
if (( $ERRNUM == 0 ))
then
	#Si s'especifica creació d'usuari...
	if [[ "$1" == "au" ]]
	then
		#Si no s'indica cap usuari o grup...
		if [[ "$3" == '' ]] || [[ "$2" == '' ]]
		then
			ERRNUM=9
		else
			#Comprovem que existeix el grup.
			comprovar_group "$3"
			if [ $? -eq 0 ]
			then
				#Si no existeix li retornarem un error a l'usuari.
				ERRNUM=10
			else
				#Comprovem si existeix l'usuari...
				comprovar_user "$2"
				if [ $? -eq 1 ]
				then
					#Si existeix li retornarem a l'usuari l'error de que no existeix.
					ERRNUM=11
				else
					#Si no existeix començarem la creació del fitxer LDIF temporal que utilitzarem per a la creció d'aquest.
					#Primer farem una réplica de la plantilla.
					cat "$RUTA_CONF"/"$ARX_LDIF_AU" > "$RUTA_CONF"/"$ARX_LDIF_TMP"
					#Li assignem l'adreça on s'ubicarà l'usuari.
					sed -i "s|^dn:.*|dn: uid=$2,ou=$U_SUFFIX,dc=$DC_SE,dc=$DC_PR|" "$RUTA_CONF"/"$ARX_LDIF_TMP"
					#Li assignarem el nom únic de l'usuari dins aquesta adreça.
					sed -i "s|^cn:.*|cn: $2|" "$RUTA_CONF"/"$ARX_LDIF_TMP"
					#Li assignem el nom comú.
					sed -i "s|^gecos:.*|gecos: $2|" "$RUTA_CONF"/"$ARX_LDIF_TMP"

					#Li assignem el ID del grup especificat amb aquesta çerca.
					AUX_GIDNUMBER=`ldapsearch -x -b cn=$3,ou=$G_SUFFIX,dc=$DC_SE,dc=$DC_PR -H $IP_DOM | grep gidNumber | cut -d ' ' -f 2`
					#Insertem el ID del grup.
					sed -i "s|^gidnumber:.*|gidnumber: $AUX_GIDNUMBER|" "$RUTA_CONF"/"$ARX_LDIF_TMP"

					#Li assignem el homedirectory.
					sed -i "s|^homedirectory:.*|homedirectory: /home/$2|" "$RUTA_CONF"/"$ARX_LDIF_TMP"
					#Li assignem el nom que utilitzarà al servidor LDAP.
					sed -i "s|^uid:.*|uid: $2|" "$RUTA_CONF"/"$ARX_LDIF_TMP"

					#Assignem el valor que retornarà la funció uidnumb amb la primer ID disponible.
					AUX_UIDNUMBER=$(uidnumb)
					#Insertem la ID amb el valor obtingut.
					sed -i "s|^uidnumber:.*|uidnumber: $AUX_UIDNUMBER|" "$RUTA_CONF"/"$ARX_LDIF_TMP"
					
					#Finalment fem la introducció de l'usuari amb l'anterior fitxer.
					ldapadd -D "cn=$DC_ADMIN,dc=$DC_SE,dc=$DC_PR" -w "$DC_PASS" -f "$RUTA_CONF"/"$ARX_LDIF_TMP"
					#-D per especificar l'usuari administrador, -w per especificar la pass i -f per especificar l'arxiu.
					if [ $? -eq 0 ]
					then
						#Si no retorna error l'execució de l'anterior comanda...
						#Esborrem el fitxer temporal.
						rm "$RUTA_CONF"/"$ARX_LDIF_TMP"
						echo "$MissExit_4" #S'ha creat l'usuari amb èxit.
						exit 0
					else
						#Si retorna error l'execució de l'anterior comanda.
						rm "$RUTA_CONF"/"$ARX_LDIF_TMP"
						ERRNUM=17
					fi
				fi
			fi
		fi
	#--- Crear grup
	#És la mateixa explicació que a la creació d'usuaris però més simple.
	elif [[ "$1" == "ag" ]]
	then
		if [[ "$2" == '' ]]
		then
			ERRNUM=9
		else
			comprovar_group "$2"
			if [ $? -eq 1 ]
			then
				ERRNUM=12
			else
				cat "$RUTA_CONF"/"$ARX_LDIF_AG" > "$RUTA_CONF"/"$ARX_LDIF_TMP"
				sed -i "s|^dn:.*|dn: cn=$2,ou=$G_SUFFIX,dc=$DC_SE,dc=$DC_PR|" "$RUTA_CONF"/"$ARX_LDIF_TMP"
				sed -i "s|^cn:.*|cn: $2|" "$RUTA_CONF"/"$ARX_LDIF_TMP"

				AUX_GIDNUMBER=$(gidnumb)
				sed -i "s|^gidnumber:.*|gidnumber: $AUX_GIDNUMBER|" "$RUTA_CONF"/"$ARX_LDIF_TMP"

				ldapadd -D "cn=$DC_ADMIN,dc=$DC_SE,dc=$DC_PR" -w "$DC_PASS" -f "$RUTA_CONF"/"$ARX_LDIF_TMP"
				if [ $? -eq 0 ]
				then
					rm "$RUTA_CONF"/"$ARX_LDIF_TMP"
					echo "$MissExit_3" #S'ha creat el grup amb èxit.
					exit 0
				else
					rm "$RUTA_CONF"/"$ARX_LDIF_TMP"
					ERRNUM=16
				fi
			fi
		fi
	#--- Eliminar usuari
	#El mateix però més simple encara que la creació de grups.
	elif [[ "$1" == "du" ]]
	then
		if [[ "$2" == '' ]]
		then
			ERRNUM=9
		else
			comprovar_user "$2"
			if [ $? -eq 0 ]
			then
				ERRNUM=13
			else
				cat "$RUTA_CONF"/"$ARX_LDIF_DU" > "$RUTA_CONF"/"$ARX_LDIF_TMP"
				sed -i "s|^dn:.*|dn: uid=$2,ou=$U_SUFFIX,dc=$DC_SE,dc=$DC_PR|" "$RUTA_CONF"/"$ARX_LDIF_TMP"

				ldapadd -D "cn=$DC_ADMIN,dc=$DC_SE,dc=$DC_PR" -w "$DC_PASS" -f "$RUTA_CONF"/"$ARX_LDIF_TMP"
				if [ $? -eq 0 ]
				then
					rm "$RUTA_CONF"/"$ARX_LDIF_TMP"
					echo "$MissExit_2" #S'ha eliminat l'usuari amb èxit.
					exit 0
				else
					rm "$RUTA_CONF"/"$ARX_LDIF_TMP"
					ERRNUM=15
				fi
			fi
		fi
	#Mateix que que la eliminació de grups.
	elif [[ "$1" == "dg" ]]
	then
		if [[ "$2" == '' ]]
		then
			ERRNUM=9
		else
			comprovar_group "$2"
			if [ $? -eq 0 ]
			then
				ERRNUM=10
			else
				cat "$RUTA_CONF"/"$ARX_LDIF_DG" > "$RUTA_CONF"/"$ARX_LDIF_TMP"
				sed -i "s|^dn:.*|dn: cn=$2,ou=$G_SUFFIX,dc=$DC_SE,dc=$DC_PR|"  "$RUTA_CONF"/"$ARX_LDIF_TMP"

				ldapadd -D "cn=$DC_ADMIN,dc=$DC_SE,dc=$DC_PR" -w "$DC_PASS" -f "$RUTA_CONF"/"$ARX_LDIF_TMP"
				if [ $? -eq 0 ]
				then
					rm "$RUTA_CONF"/"$ARX_LDIF_TMP"
					echo "$MissExit_1" #S'ha eliminat el grup amb èxit.
					exit 0
				else
					rm "$RUTA_CONF"/"$ARX_LDIF_TMP"
					ERRNUM=14
				fi
			fi
		fi
	fi
fi
#------------------------------------------------------------------------



#------------------------------------------------------------------------
#--- Comprovació si s'ha especificat argument
#---  MÒDUL 99
#------------------------------------------------------------------------
#Si no s'ha especificat cap argument arribarà aquí, no volem que aparegui aquest missatge
#	per pantalla si s'ha configurat l'arxiu de configuració ja que aquest no requereïx
#	cap argument per fer-ho possible.
if (( $ERRNUM == 0 )) && (( $CHECK_CONF == 0 ))
then
	ERRNUM=20
fi
#------------------------------------------------------------------------



# -----------------------------------------
# ------------- ERRORCODE -----------------
# -----------------------------------------

if (( $ERRNUM == 1 ))
then
	echo "$MissErr_1" #Error[1] -> No s'ha executat aquest script com a root.
elif (( $ERRNUM == 2 ))
then
	echo "$MissErr_2" #Error[2] -> Introdueïx un paràmetre al programa.
elif (( $ERRNUM == 3 ))
then
	echo "$MissErr_3" #Error[3] -> No es pot determinar el domini assignat per defecte.
elif (( $ERRNUM == 4 ))
then
	echo "$MissErr_4" #Error[4] -> Ja existeix l'arxiu de configuració, si vols borrar-ho executa l'script en mode desinstal·lació.
elif (( $ERRNUM == 5 ))
then
	echo "$MissErr_5" #Error[5] -> No s'han eliminat els següents fitxers perque no existeixen:
	#Mostrem el contingut de l'arxiu on s'han depositat els noms de fitxers.
	echo `cat "$ARX_RM_TMP"`
	#Esborrem l'arxiu per no deixar-hi residu.
	rm "$ARX_RM_TMP"
elif (( $ERRNUM == 6 ))
then
	echo "$MissErr_6" #Error[6] -> El programa no està instal·lat.
elif (( $ERRNUM == 7 ))
then
	echo "$MissErr_7 $DPKG" #Error[7] -> Paquet indispensable no instal·lat:
elif (( $ERRNUM == 8 ))
then
	echo "$MissErr_8" #Error[8] -> No s'ha creat la carpeta on depositar els fitxers.
elif (( $ERRNUM == 9 ))
then
	echo "$MissErr_9" #Error[9] -> Falta algun paràmetre.
elif (( $ERRNUM == 10 ))
then
	echo "$MissErr_10" #Error[10] -> No existeix el grup introduït.
elif (( $ERRNUM == 11 ))
then
	echo "$MissErr_11" #Error[11] -> Ja existeix l'usuari introduït.
elif (( $ERRNUM == 12 ))
then
	echo "$MissErr_12" #Error[12] -> Ja existeix el grup introduït.
elif (( $ERRNUM == 13 ))
then
	echo "$MissErr_13" #Error[13] -> No existeix l'usuari introduït.
elif (( $ERRNUM == 14 ))
then
	echo "$MissErr_14" #Error[14] -> No s'ha pogut esborrar el grup.
elif (( $ERRNUM == 15 ))
then
	echo "$MissErr_15" #Error[15] -> No s'ha pogut esborrar l'usuari.
elif (( $ERRNUM == 16 ))
then
	echo "$MissErr_16" #Error[16] -> No s'ha pogut crear el grup.
elif (( $ERRNUM == 17 ))
then
	echo "$MissErr_17" #Error[17] -> No s'ha pogut crear l'usuari.
elif (( $ERRNUM == 18 ))
then
	echo "$MissErr_18" #Error[18] -> No s'ha pogut canviar la IP del servidor LDAP.
elif (( $ERRNUM == 19 ))
then
	echo "$MissErr_19" #Error[19] -> No s'ha pogut canviar el domini.
elif (( $ERRNUM == 20 ))
then
	echo "$MissErr_20" #Error[20] -> Introdueïx un paràmetre correcte.
fi
