#! /bin/bash
#DESCRIPCIÓ:
#Creador d'arxius Grans
#
#	Tenir en compte que el nom dels fixters és un número començant des de 0 i sumant-li +1.
#
#AUTORS: Nil Llisterri - Adrian Martínez

#-----------------------
#-------SOFTCODE--------
#-----------------------

#Noms dels fitxers
MB100=0
MB300=0
MB500=0

#-----------------------
#-------HARDCODE--------
#-----------------------

#Comprovació de si existeix la carpeta, si no, la crea. "/media/NTFS_FS/Petits" per exemple.
if ! [ -d "$1" ]
then
	mkdir "$1"
fi

#Aquí comença la comprovació de on s'ha cancel·lat l'script anteriorment.
#Definirà les variables de nom de fitxers a l'últim número trobat com a nom de fitxer.
#Un per a cada mida.
for k in 100 300 500
do

	#Si existeix la carpeta entra a veure el contingut. Si no, salta aquest pas i passa a la següent mida.
	if [ -d "$1"/ARXIUS$k\MB ]
	then
	
		#Donem per supossat que l'usuari no ha creat més de 500 arxius adicionals amb un nom diferent. En cas de que sigui així
		#	cambiarem aquest número.
		for e in {1..500}
		do
		
			#Aquí és on salta els arxius amb un nom diferent a un número, si no és 0 pot ser 10 si no 100 si no 1000
			#	suporta un màxim de 3 xifres, 5 xifres per a l'script creador de petits.
			#Bàsicament el que fa aquesta comprovació és:
			# 1- Fes un ls llistant un fitxer per cada linia. Aquests resultats classifical's per ordre natural (001 és més petit que 8).
			# 2- Retalla el nombre de linies especificades al for començant per aball.
			# 3- Retalla i agafa la primera linia.
			# Pas final- Aquest resultat és un número de 1,2,3 o 4 xifres? R: No, afegeix una linia més de marge al pas 2.
			#Per naturalesa els fitxers amb caràcters i no lletres es mostren al final si ho ordenem així que no dona errors.
			if [[ `ls -1v "$1"/ARXIUS"$k"MB | tail -n "$e" | head -n 1` = [0-9] ]] || [[ `ls -1v "$1"/ARXIUS"$k"MB | tail -n "$e" | head -n 1` = [0-9][0-9] ]] || [[ `ls -1v "$1"/ARXIUS"$k"MB | tail -n "$e" | head -n 1` = [0-9][0-9][0-9] ]]
			then
			
				#R: Si, declara la variable MB$k (necessaria la declaració "declare" per definir variables compostes d'altres) 
				#	amb el resultat obtingut abans.
				declare MB$k=`ls -1v "$1"/ARXIUS"$k"MB | tail -n "$e" | head -n 1`
				break
			fi
		done
	fi
done

#Mentre el tamany de la carpeta passada com a argument sigui menor al
#	tamany passat com a argument segueix fent-lo.
while (( `du -s "$1" | cut -f 1` <= $2))
do

	#Demana un random de entre 0 i 2 que és el número de tamanys a crear.
	Rand=$((RANDOM%3))
	
	#Segons el random qur surti agafarà un tamany concret
	#	al pròxim arxiu a crear.
	if (( $Rand==0))
	then
		ESPAI=100
	fi
	if (( $Rand==1))
	then
		ESPAI=300
	fi
	if (( $Rand==2))
	then
		ESPAI=500
	fi
	
	#Si no existeix la carpeta de l'arxiu en concret, crea-la.
	#La ruta és la definida com a argument /ARXIUS + espai + MB
	if ! [ -d "$1"/ARXIUS$ESPAI\MB ]
	then
		mkdir "$1"/ARXIUS$ESPAI\MB
	fi
	
	#Crea el fitxer del tamany escollit a l'altzar amb un tamany de bloc d'1MB
	#Anomena-lo amb la variable que és va autoincrementant
	#	la variable és defineix amb la suma de l'string MB + el tamany
	#	escollit a l'atzar.
	dd if=/dev/zero of="$1"/ARXIUS$ESPAI\MB/$[MB$ESPAI] bs=1048576 count=$ESPAI &> /dev/null
	let "MB$ESPAI += 1"
done
