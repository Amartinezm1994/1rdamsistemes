#! /bin/bash
#AUTORS:
#	Adrian Martínez
#	Nil Llisterri

#
#         Idea bàsica de l'script:
# L'script té dos modes, el mode normal on fa arxius fins a 7GB, petits i grans en totes les unitats:
# // NTFS // XFS // REISER // EXT4 //
# Si no troba qualsevol d'aquestes unitats el programa mostra un missatge de error i no fa res.
# Si no hi ha més de 7GB d'espai al disc (tot el disc, no que la carpeta ocupi més de 7GB) dona error
#	i finalitza el programa. Si hi han escrits 8GB i el disc té mida total de 16GB el programa procedirà.
# Si els discs no estàn en sdb1, sdc1, sdd1 i sde1 el programa donarà missatge de error i es cancel·larà.
# A més el programa t'informa de quina unitat és la que ha donat la fallada.
# Els dos arxius a tmp és sobreescriuràn cada vegada que executem el programa.
# No importa quin sigui l'ordre de muntatge, l'script ja el detecta (sdb1 amb EXT4 o sdc1 amb REISER per exemple).
#
# 	Mode recuperació:
# Hem implementat un mode de recuperació que consisteix en reanudar la creació d'arxius.
# Si ha hagut cap fallada al sistema i s'ha tingut que cancel·lar el programa, al iniciar
#	una altre vegada el programa ens donarà l'opció d'entrar en mode recuperació, analitzarà
#	les carpetes i arxius que hi han i posarà les variables tal i com estaven al cancel·lar el programa
#	(per a provar-ho es convenient tancar la finestra del terminal i no cancel·lar amb Ctrl + C).
# Si per exemple has escrit un arxiu amb un nom qualsevol a la carpeta on es generen els arxius
#	el programa no l'agafarà i saltarà fins l'últim arxiu creat pel programa.
#
#		Inconvenients de l'script:
# No és compatible amb altres sistemes de fitxers.
# El mode recuperació no et recupera el temps transcorregut a l'última execució cancel·lada de l'script.
#
# ---------------------------------------------------
# -		IMPORTANT!									-
# - Hi han més notes als arxius fills de l'script	-
# - on hi ha més codi per al procés de recuperació.	-
# - Així com més notes descriptives.				-
# ---------------------------------------------------

#----------------------
#-------SOFTCODE-------
#----------------------

#Podem modificar la carpeta on és guarden els arxius d'informació de temps.
RUTATMP="/tmp"

#Podem modificar l'espai a escriure en el disc.
ESPAIMAXIM="7000000" #En KB

#Podem modificar i traduïr els missatges d'informació de l'script.
MisPrin="Ha iniciat l'script."
MisFin="L'script ha finalitzat."
MisErrorCar="Carpeta introduïda inexistent."
MisErrorEspai="No hi ha espai suficient a aquesta unitat:"
MisErrorUCar="No existeix la següent unitat:"
MisErrorUSel="No hi ha espai suficient en la unitat a recuperar:"
MisErrorUNo="El punt on es troba ubicada la unitat no es troba (sdb1,sdc1,sdd1,sde1)"
MisErrorPerm="No hi han permisos en la següent unitat:"
MisErrorFills="No existeix el següent script imprescindible:"

#----------------------
#-------HARDCODE-------
#----------------------

#Inicialitzem la variable AUX a 0 per evitar problemes de lectura
AUX=0

#Comprovem que existeix aquesta carpeta, si no no fa cap cosa i mostra un error
if [ -d "$1" ]
then
	
	#Comprovem que existeixen els dos scripts fills.
	for v in Petits Grans
	do
		if ! [ -e creador"$v".sh ]
		then
			AUX=6
			break
		fi
	done
	
	#Inicialitzem la variable de opcio per poguer-la comparar i evitar redundància
	opcio=0
	while (( $opcio < 1 || $opcio > 2 ))
	do
		
		#Donem a l'usuari per escollir quin en quin mode iniciar el programa, si no hi ha res a recuperar
		#	i li donem a mode recuperació el programa començarà de nou com si li donéssim mode normal.
		read -p "Entrar en mode recuperació? (Reanudarà l'anterior operació) [1 - SI / 2 - NO]" opcio
		if (( $opcio < 1 || $opcio > 2 ))
		then
			echo "Introdueïx una opció vàlida."
		fi
	#Controlem que hi introdueixi una opció vàlida
	done
	
	#Inicialitzem compr a 0
	compr=0
	
	#Si escollim mode recuperació passarà per aquí per analitzar a quina carpeta s'ha quedat quan s'ha cancel·lat
	#	el programa.
	if (( $opcio==1 ))
	then
		for m in Petits Grans
		do
			for l in NTFS REISER XFS EXT4
			do
				
				#Quan trobi una carpeta amb el nom */(NTFS || REISER || XFS || EXT4_FS /(Petits || Grans)
				#	trenca el bucle i posa el compr a 1.
				#Si hi ha una carpeta amb aquest nom significa que aquí s'ha deixat l'anterior operació del programa
				if [ -d "$1"/"$l"_FS/"$m" ]
				then
					compr=1
					break
				fi
			done
			
			#Si ja ha trobat la carpeta no volem que sigui fent el bucle.
			if (( $compr==1 ))
			then
				break
			fi
		done
		
	#Si no escollim mode recuperació inicialitzem aquestes variables.
	else
		l=NTFS
		m=Petits
	fi
	
	#Inicialitzem aquestes dues variables.
	compro=0
	comprov=0
	
	#Aquí comença la comprovació d'espai en el disc dur passant per cada sistema de fitxers
	for k in NTFS REISER XFS EXT4
	do
		
		#Si no existeixen les 4 carpetes on és munten els discs donarà missatge d'error
		#	amb posterior cancel·lació de l'script. Agafem la unitat on dona error per mostrar-la posteriorment.
		if ! [ -d "$1"/"$k"_FS ]
		then
			AUX=2
			UNITAT=$k
			break
		fi
		
		#Comprovem que tenim permissos d'escriptura a les 4 carpetes dels sistemes de fitxers.
		if ! [ -w "$1"/"$k"_FS ]
		then
			AUX=5
			UNITAT=$k
			break
		fi
		
		#Si el nom de la variable del for actual i el nom de la variable abans analitzada en mode recuperació
		#	passa per aquí per possar la variable de comprovació a 1
		if [ "$l" == "$k" ]
		then
			compro=1
		fi
		
		#Si la comprovació de abans s'ha fet efectiva vol dir que pot començar a analitzar aquest disc dur
		#	no volem que analitzi aquest disc dur perquè si s'ha cance·lat aquí vol dir que si hi haurà
		#	espai però l'espai que ja hi ha ocupat és de l'script, per tant no passis per aquí.
		#	Passa en el següent loop del for on si caldrà analitzar si hi ha espai disponible (en els següents discs).
		if (( $compro==1)) && [ "$l" != "$k" ]
		then
			comprov=1
		fi
		
		#Si no ha trobat cap carpeta amb cap nom estipulat per l'script o s'ha sel·leccionat mode normal
		#	passa per aqui per analitzar tots els discs. Si per el contrari s'ha sel·leccionat mode
		#	recuperació no cal analitzar els discs anteriors, per tant passarà per aquí si la comprovació
		#	anterior és certa.
		if  (( $compr==0 )) || (( $comprov==1 ))
		then
		
			#El que fa això és classificar les particiones amb el seu nom d'arxiu
			#	per exemple: NTFS amb sdb1, REISER amb sde1
			#	això amb el supòsit de que el nombre de partició sigui 1
			#	si no existeix dona un missatge d'error de que no existeix i cancel·la l'script.
			#No hem trobat la manera de afegir-hi una variable dins del programa awk
			#	si no, ho haguerem fet tot amb un for i la lletra i no hi hauria tanta redundància.
			#El que fa aquest awk és detectar el nom on està muntada la partició i talla 
			#	(/media/NTFS_FS) agafa NTFS i el compara si correspon amb la variable del for actual
			#	si ho és defineix la variable "n" i defineix-la com a lletra de disc.
			if [[ `df | awk '$1=="/dev/sdb1"{print $6}' | cut -d '/' -f 3 | cut -d '_' -f 1` = $k ]]
			then
				n=b
			elif [[ `df | awk '$1=="/dev/sdc1"{print $6}' | cut -d '/' -f 3 | cut -d '_' -f 1` = $k ]]
			then
				n=c
			elif [[ `df | awk '$1=="/dev/sdd1"{print $6}' | cut -d '/' -f 3 | cut -d '_' -f 1` = $k ]]
			then
				n=d
			elif [[ `df | awk '$1=="/dev/sde1"{print $6}' | cut -d '/' -f 3 | cut -d '_' -f 1` = $k ]]
			then
				n=e

			#En cas de que no correspongui la lletra del disc amb cap sistema de fitxers mostrarà un error de partició
			#	i cancel·larà l'script.
			else
				AUX=4
			fi
			
			#Més redundància per no poguer encaixar una variable dins del programa awk.
			#Si la lletra de partició correspon amb b, c, d o e defineix una variable amb l'espai lliure del disc.
			#	compara aquest espai amb el màxim definit al principi de l'script.
			#Això per cada lletra de disc.
			if [ "$n" == "b" ]
			then
				LLESPb=`df | awk '$1=="/dev/sdb1"{print $4}'`
				if (( $LLESPb <= $ESPAIMAXIM ))
				then
					AUX=1
					UNITAT=$k
					break
				fi
			elif [ "$n" == "c" ]
			then
				LLESPc=`df | awk '$1=="/dev/sdc1"{print $4}'`
				if (( $LLESPc <= $ESPAIMAXIM ))
				then
					AUX=1
					UNITAT=$k
					break
				fi
			elif [ "$n" == "d" ]
			then
				LLESPd=`df | awk '$1=="/dev/sdd1"{print $4}'`
				if (( $LLESPd <= $ESPAIMAXIM ))
				then
					AUX=1
					UNITAT=$k
					break
				fi
			elif [ "$n" == "e" ]
			then
				LLESPe=`df | awk '$1=="/dev/sde1"{print $4}'`
				if (( $LLESPe <= $ESPAIMAXIM ))
				then
					AUX=1
					UNITAT=$k
					break
				fi
			fi
		fi
	done
	
	#Si no ha donat cap error procedirà a la creació d'arxius.
	if (( $AUX==0 ))
	then
		
		#Missatge de començament d'execució.
		echo "$MisPrin"
		
		#For d'arxius petits i grans
		for i in Petits Grans
		do
		
			#For de cada sistema de fitxers
			for k in NTFS REISER XFS EXT4
			do
				
				#Si l'script s'ha iniciat en mode normal o el mode recuperació ha trobat cap indici d'aturada
				#	de l'script en l'anterior execució procedirà a crear la carpeta i el fitxer de text
				#	on s'emmagatzemaràn les dades.
				if (( $compr==0 ))
				then
				
					#Indica la primera variable del for de sistema de fitxers, si ho és crea el fitxer de text
					#	i si ja existeix la carpeta esborra-la. En mode de recuperació amb indici de cancel·lació
					#	no passarà per aquí i no donarà conflictes.
					if [ "$k" == "NTFS" ]
					then
						echo "----Arxius $i----" > "$RUTATMP"/dades"$i".txt
						if [ -d "$1"/NTFS_FS/"$i" ]
						then
							rm -R "$1"/NTFS_FS/"$i"
						fi
					fi
					
					#Classifiquem en el arxiu de text cada sistema de fixters per a fer més comprensible la seva lectura.
					echo "----Creació $k----" >> "$RUTATMP"/dades"$i".txt
					
					#Hem tingut molts problemes per a utilitzar el programa /usr/bin/time llavors hem acabat definint
					#	una variable amb la comanda a executar.
					ARGUMENT="./creador$i.sh $1/$k\_FS/$i $ESPAIMAXIM"
					
					#Per a guardar la informació d'aquest programa és necessari passar-li com a paràmetre un "output file"
					#	on depossiti la informació el -p és per fer una lectura més cómoda.
					#Per fer que interprèti correctament la comanda és té que possar "bash -c" per a què ho llegeixi amb
					#	l'intèrpret bash "-c" és de command. Li donarem com a comanda l'anterior variable definida.
					/usr/bin/time -p -o "$RUTATMP/dades.txt" bash -c "$ARGUMENT"
					
					#Transportem el contingut d'aquest fitxer al fitxer de dades definitiu de l'script.
					cat "$RUTATMP/dades.txt" >> "$RUTATMP/dades$i.txt"
					
					#Esborrem la carpeta i els arxius creats per l'script.
					rm -R "$1"/"$k"_FS/"$i"
					
				#En mode recuperació amb indicis de cancel·lació començarà a fer la creació d'arxius quan les variables on comprobaven
				#	si existía alguna carpeta quan coïncideixin amb les d'aquests dos fors. Per exemple si s'ha cancel·lat l'script
				#	al sistema de fitxers NTFS als arxius Grans, per aquí no passarà fins que la variable i no sigui igual a la variable
				#	m (Petits o Grans) i la variable l no sigui igual a k (NTFS, XFS...). Amb això reanudem l'script desde la vista
				#	de carpetes. Per veure el procés de reanudació de fitxers consultar els scripts fills, allà hi ha una explicació.
				elif [ "$m" == "$i" ] && [ "$l" == "$k" ]
				then
				
					#Es salta la creació del fitxer de text i el esborrat de la carpeta. Tota la resta és el mateix que l'apartat anterior.
					ARGUMENT="./creador$i.sh $1/$k\_FS/$i $ESPAIMAXIM"
					/usr/bin/time -p -o "$RUTATMP/dades.txt" bash -c "$ARGUMENT"
					cat "$RUTATMP/dades.txt" >> "$RUTATMP/dades$i.txt"
					rm -R "$1"/"$k"_FS/"$i"
					compr=0
				fi
			done
		done
		
		#Missatge de finalitzat, aquí acaba tot si ha sortit tot bé.
		echo "$MisFin"
		
	#Aquí comença la secció de missatges d'error, la variable AUX adopta un valor different
	#	segons quin error hagi sortit.
	else
	
		#Si no ha hagut prou espai mostra aquest missatge d'error i la unitat on ha sorgit aquest.
		if (( $AUX==1 ))
		then
			echo "$MisErrorEspai $UNITAT"
			
		#Si no existeix la carpeta on està muntada la unitat i la unitat on no hi és.
		elif (( $AUX==2 ))
		then
			echo "$MisErrorUCar $UNITAT"
			
		#No passarà per aquí perquè no hem implementat cap sistema que et digui si hi ha espai suficient en la unitat on és farà la recuperació
		#	calculant: espai lliure - (espai que ocuparà - espai actual dels fitxers)
		elif (( $AUX==3 ))
		then
			echo "$MisErrorUSel $UNITAT"
			
		#Si sdb,sdc... no és troba.
		elif (( $AUX==4 ))
		then
			echo "$MisErrorUNo"
		
		#Missatge d'error quan no hi tenim permissos d'escriptura i a quina unitat.
		elif (( $AUX==5 ))
		then
			echo "$MisErrorPerm $UNITAT"
		
		#Missatge d'error quan no es troben qualsevol dels dos fitxers fills de l'script.
		else
			echo "$MisErrorFills creador$v.sh"
		fi
	fi
else
	echo "$MisErrorCar"
fi