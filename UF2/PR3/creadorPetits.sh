#! /bin/bash
#DESCRIPCI�:
#Creador d'arxius Grans
#
#	
#
#AUTORS: Nil Llisterri - Adrian Mart�nez



# IMPORTANT -> Les explicacions dels scripts fills est�n al creadorGrans.sh

#-----------------------
#-------SOFTCODE--------
#-----------------------

MB2=0
MB3=0
MB7=0

#-----------------------
#-------HARDCODE--------
#-----------------------

if ! [ -d "$1" ]
then
	mkdir "$1"
fi

for k in 2 3 7
do
	if [ -d "$1"/ARXIUS$k\MB ]
	then
		for e in {1..500}
		do
			if [[ `ls -1v "$1"/ARXIUS"$k"MB | tail -n "$e" | head -n 1` = [0-9] ]] || [[ `ls -1v "$1"/ARXIUS"$k"MB | tail -n "$e" | head -n 1` = [0-9][0-9] ]] || [[ `ls -1v "$1"/ARXIUS"$k"MB | tail -n "$e" | head -n 1` = [0-9][0-9][0-9] ]] || [[ `ls -1v "$1"/ARXIUS"$k"MB | tail -n "$e" | head -n 1` = [0-9][0-9][0-9][0-9] ]] || [[ `ls -1v "$1"/ARXIUS"$k"MB | tail -n "$e" | head -n 1` = [0-9][0-9][0-9][0-9][0-9] ]]
			then
				declare MB$k=`ls -1v "$1"/ARXIUS"$k"MB | tail -n "$e" | head -n 1`
				break
			fi
		done
	fi
done

while (( `du -s "$1" | cut -f 1` <= $2))
do
	Rand=$((RANDOM%3))
	if (( $Rand==0))
	then
		ESPAI=2
	elif (( $Rand==1))
	then
		ESPAI=3
	else
		ESPAI=7
	fi
	if ! [ -d "$1"/ARXIUS$ESPAI\MB ]
	then
		mkdir "$1"/ARXIUS$ESPAI\MB
	fi
	dd if=/dev/zero of="$1"/ARXIUS$ESPAI\MB/$[MB$ESPAI] bs=1048576 count=$ESPAI &> /dev/null
	let "MB$ESPAI += 1"
done