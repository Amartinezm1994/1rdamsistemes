----Arxius Petits----
----Creaci� NTFS----
real 202.20
user 13.80
sys 44.46
----Creaci� REISER----
real 251.99
user 10.90
sys 24.17
----Creaci� XFS----
real 45.14
user 11.77
sys 17.25
----Creaci� EXT4----
real 33.46
user 11.12
sys 21.09

# Arxius petits, blocs d'1MB en arxius de 2,3 i 7 MB amb un total de 7GB per cada disc.
# Reiser �s el que triga m�s, triga molt a fer l'escriptura a disc. NTFS �s el que triga m�s a processar les dades a sistema.
# EXT4 �s el que triga menys en general. XFS �s el que triga menys a processar les dades al sistema.
# Conclusi�: EXT4 t� millor rendiment general.