----Arxius Grans----
----Creaci� NTFS----
real 113.02
user 0.20
sys 21.99
----Creaci� REISER----
real 175.24
user 0.17
sys 14.58
----Creaci� XFS----
real 27.89
user 0.19
sys 8.05
----Creaci� EXT4----
real 23.50
user 0.24
sys 9.26

# Com veiem, les unitats son en segons, blocs d'1MB i 7GB a cada disc dur. Arxius de 100,300 i 500MB.
# Reiser �s el que triga m�s amb diferencia, el que tarda es a escriure en disc.
# NTFS el segueix fent m�s carga al sistema que qualsevol del altres FSs. EXT4 �s el m�s r�pid. XFS �s el que triga menys en sistema.
# Conclusi�: EXT4 t� el millor rendiment en general.