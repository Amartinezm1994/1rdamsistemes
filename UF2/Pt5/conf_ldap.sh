﻿#! /bin/bash

#--------------------------------------------------------------------------------------------
#AUTOR:
# - Adrian Martínez Montes
#--------------------------------------------------------------------------------------------


#--------------------------------------------------------------------------------------------
#Descripció:
# - Funció principal:
# Fa l'autentificació de la mateixa màquina a un servidor LDAP amb la IP especificada.

# - Funció secundaria:
# Si tens una instal·lació corrupta del libnss-ldap aquest script pot reparar-la, per tant
# podem executar aquest script totes les vegades que vulguem.

# - Organització de l'script:
# Script basat en mòduls, es poden esborrar diversos mòduls sense que afecti al rendiment
# de l'script, es poden afegir mòduls sense afectar al rendiment de l'script, si s'afegeix
# un mòdul de comprovació es recomanable deixar un missatge d'error al mòdul 00 i definir
# un nombre més alt per a la variable "AUX".
# ATENCIÓ: potser que si s'esborra un mòdul concret l'script pot deixar de funcionar.
# Mòduls no recomanats d'esborrar: 06 i 07.
# La raó de la no recomanació d'esborrat d'aquests dos mòduls és que són el "core" de l'script,
# sense aquests dos mòduls l'script deixa de fer la seva funció principal, que és l'autenticació
# contra un servidor LDAP.

# - Comprovacions d'aquest script:
# MODUL01 - 01. Comprova si és root.
# MODUL02 - 02. Comprova versió d'ubuntu.
# MODUL03 - 03. Comprova disponibilitat del servidor especificat per IP.
# MODUL04 - 04. Comprova connexió a internet.
# MODUL04 - 05. Comprova connexió amb el servidor web LDAP.
# MODUL05 - 06. Comprova si libnss-ldap està instal·lat.
# MODUL06 - 07. Comprova que no hi hagi cap error en la descàrrega de la plantilla de configuració LDAP.
# MODUL06 - 08. Comprova que no hi hagi cap error en la instal·lació del paquet libnss-ldap.
# MODUL07 - 09. Comprova que no hi hagi cap error en la descàrrega de la plantilla de configuració del pam.
# MODUL07 - 10. Comprova que no hi hagi cap error en la reconfiguració del pam.
# MODUL08 - 11. Comprova que existeixi el fitxer lightdm per no llistar usuaris a la pantalla de login.
# MODUL09 - 12. Comprova que el fitxer de login del pam existeixi per aplicar la creació del home.
# MODUL09 - 13. Comprova que el fitxer de lightdm del pam existeixi per aplicar la creació del home per gràfica.

# - Script descriptiu:
# Tot el que fa l'script es mostra per pantalla, i si hi ha qualsevol error es mostrarà un missatge
# d'error amb el codi d'error (explicat més extensament al manual) i una breu descripció del que
# l'ha ocasionat.

# - Joc amb les plantilles de configuració:
# La funcionalitat d'aquest script es bassa en l'aplicació de plantilles de configuració en
# les instal·lacions, les descarrega del servidor LDAP especificat i després les aplica. Per
# esmentar-les correctament les anomenem "seeds", que són semilles, i normalment van en arxius
# d'extensió .seed , encara que he volgut fer-les en arxius de text per poder ser llegides fàcilment.

# - Si es fa anar una altre interfície gràfica:
# El primer buscar els arxius on es configura l'accés a l'autenticació, per exemple, amb la creació del
# home amb gnome la ruta pot ser: /etc/pam.d/gdm. Llavors tindrem que modificar-hi la variable PAM_LIGHTDM
# amb aquesta ruta. També haurem de modificar la variable LIGHTDM_CONF amb la ruta de la pròpia interfície
# gràfica on hi hagin les configuracions, aquesta variable és per modificar el llistat d'usuaris a al fer
# login gràficament.

# - Limitacions de l'script:
# 1. No té suport per configurar-hi l'autenticació en un altre domini que no sigui el que ja vé en la
#		plantilla descarregada en el propi servidor, si es vol cambiar de domini, parlar amb el proveïdor
#		del servidor LDAP.
# 2. No té suport per definir els tipus concrets de sistema d'autenticació, com per exemple que només
#		autentiqui contra el servidor LDAP i no a la pròpia màquina amb sistema UNIX. Això ve definit
#		a la plantilla de configuració del pam, parlar amb el proveïdor del servidor LDAP per poguer
#		fer aquest canvi.
# 3. No és recomanable modificar la versió de l'Ubuntu per fer còrrer aquest script ja que no ha sigut
#		testejat en una altre versió d'ubuntu que no hagi sigut la 12.04, a més potser que determinats
#		fitxers no es trobin disponibles en altres versions d'Ubuntu i doni error. Es posible també
#		que es necessitin altres configuracions i afegir altres opcions per fer l'autenticació correctament,
#		en aquest cas afegir un altre mòdul a l'script fent aquesta funció.

# - Si l'administrador del servidor LDAP canvia les plantilles:
# Podem modificar el nom d'aquestes plantilles que descarreguem del servidor LDAP si per exemple l'administrador
# les ha modificat, només tenim que definir les variables en la secció del SOFTCODE amb el nom dels fitxers
# de plantilla.

# - Recomanacions si es vol fer anar aquest script en un altre versió d'Ubuntu:
# 1. Cambiar la variable en el softcode amb el número de versió en el que faràs còrrer aquest script.
# 2. Si no es troba qualsevol dels arxius de configuració pots editar-ho a la part del softcode
#		indicant-hi la ruta d'aquests.
# 3. Si es necessita la configuració de fitxers addicionals pots incloure nous mòduls a l'script
#		amb la fi d'aconseguir fer tot automàtic.
#--------------------------------------------------------------------------------------------


#----------------------------
#---------SOFTCODE-----------
#----------------------------

SEED_LDAP="seed_ldap.txt"
SEED_PAM="seed_pam.txt"
LIGHTDM_CONF="/etc/lightdm/lightdm.conf"
PAM_LOGIN="/etc/pam.d/login"
PAM_LIGHTDM="/etc/pam.d/lightdm"


#Versió Ubuntu (no recomanable modificar-ho)
UBU_V="12.04"

#----------------------------
#---------HARDCODE-----------
#----------------------------

IPLDAP="$1"
AUX=0

#------- MODUL #01 --------------------------------------------------------------------------
#--- Comprovació si s'executa com a root.
printf "\n -> Comprovació root... "
if (( `id -u` != 0 ))
then
	AUX=1
	printf " error.\n"
else
	printf " OK.\n"
fi
#--------------------------------------------------------------------------------------------



#------- MODUL #02 --------------------------------------------------------------------------
#--- Comprovació versió Ubuntu que executa l'script.
if (( $AUX == 0 ))
then
	printf " -> Comprovant versió d'Ubuntu actual ..."
	if [[ `lsb_release -a 2> /dev/null | grep Release | cut -f 2` = "$UBU_V" ]]
	then
		printf " OK.\n"
	else
		AUX=6
		printf " error.\n"
	fi
fi
#--------------------------------------------------------------------------------------------



#------- MODUL #03 --------------------------------------------------------------------------
#--- Comprovació si el servidor LDAP introduït té connexió.
if (( $AUX == 0 ))
then
	printf " -> Comprovant connectivitat amb el servidor LDAP ... "
	ping -c 5 $IPLDAP &> /dev/null
	if [[ $? -eq 0 ]]
	then
		printf "OK.\n"
	else
		AUX=8
		printf "error.\n"
	fi
fi
#--------------------------------------------------------------------------------------------



#------- MODUL #04 --------------------------------------------------------------------------
#--- Comprovació si hi ha connexió a internet i amb el servidor web del LDAP.
if (( $AUX == 0 ))
then
	printf " -> Establint connexió amb els servidors de descàrrega ...\n"
	printf "    Connexió a internet ... "
	wget -q --tries=10 --timeout=20 http://google.com
	if [[ $? -eq 0 ]]
	then
		printf "OK.\n"
		printf "    Connexió al servidor web del LDAP ... "
		wget -q --tries=10 --timeout=20 http://$IPLDAP
		if [[ $? -eq 0 ]]
		then
			printf "OK.\n"
		else
			AUX=3
			printf "error.\n"
		fi
	else
		AUX=2
		printf "error.\n"
	fi
fi
#--------------------------------------------------------------------------------------------



#------- MODUL #05 --------------------------------------------------------------------------
#--- Comprovació si libnss-ldap està instal·lat.
if (( $AUX == 0 ))
then
	printf " -> Comprovació si el programa libnss-ldap està instal·lat.\n"
	if [[ `dpkg --get-selections libnss-ldap 2> /dev/null | cut -f 1` = 'libnss-ldap' ]]
	then
		printf "    Programa instal·lat.\n"
		while [[ $CONF != 's' ]] && [[ $CONF != 'n' ]] && [[ $CONF != 'S' ]] && [[ $CONF != 'N' ]]
		do
			read -p "    Vols re-instal·lar el programari? [S o N] " CONF
			if [[ $CONF == "S" ]] || [[ $CONF == "s" ]]
			then
				printf " -> Eliminant fitxers del paquet libnss-ldap ... "
				apt-get purge -qq --force-yes libnss-ldap &> /dev/null
				apt-get remove -qq --force-yes libnss-ldap &> /dev/null
				printf "OK.\n"
			elif [[ $OPCIO == "N" ]] || [[ $OPCIO == "n" ]]
			then
				AUX=7
			else
				printf "    Error, introdueïx una opció correcta.\n"
			fi
		done
	fi
fi
#--------------------------------------------------------------------------------------------



#------- MODUL #06 --------------------------------------------------------------------------
#--- Agafa la plantilla de configuració LDAP i aplica la configuració d'IP a la instal·lació.
if (( $AUX == 0 ))
then
	printf "    Programa no instal·lat, procedint a instal·lar-lo.\n"
	printf " -> Descarregant plantilla de configuració ..."
	wget -q http://$IPLDAP/"$SEED_LDAP"
	if [[ $? -eq 0 ]]
	then
		printf "OK.\n"
		printf " -> Aplicant configuració a la plantilla ... "
		echo "ldap-auth-config    ldap-auth-config/ldapns/ldap-server    string    ldap://$IPLDAP/" >> "$SEED_LDAP"
		printf "OK.\n"
		printf " -> Descarregant i instal·lant el programari: libnss-ldap ... "
		cat "$SEED_LDAP" | debconf-set-selections
		DEBIAN_FRONTEND=noninteractive apt-get install -qq --force-yes libnss-ldap &> /dev/null
		if [[ $? -eq 0 ]]
		then
			printf "OK.\n"
		else
			AUX=5
			printf "error.\n"
		fi
	else
		AUX=4
		printf "error.\n"
	fi
	rm "$SEED_LDAP"
fi
#--------------------------------------------------------------------------------------------



#------- MODUL #07 --------------------------------------------------------------------------
#--- Aplica les configuracions per al NSS i el pam.
if (( $AUX == 0 ))
then
	printf " -> Configurant perfil LDAP per a NSS ... "
	auth-client-config -t nss -p lac_ldap
	printf " OK.\n"
	printf " -> Configurant sistema per l'autenticació LDAP.\n"
	printf "    Descarregant plantilla ... "
	wget -q http://$IPLDAP/"$SEED_PAM"
	if [[ $? -eq 0 ]]
	then
		printf "OK.\n"
		printf "    Aplicant configuració d'autenticació ... "
		cat "$SEED_PAM" | debconf-set-selections
		DEBIAN_FRONTEND=noninteractive pam-auth-update
		if [[ $? -eq 0 ]]
		then
			printf "OK.\n"
		else
			AUX=10
			printf "error.\n"
		fi
	else
		AUX=9
		printf "error.\n"
	fi
	rm "$SEED_PAM"
fi
#--------------------------------------------------------------------------------------------



#------- MODUL #08 --------------------------------------------------------------------------
#--- Comprovació i aplicació per amagar els usuaris a la pantalla de login.
if (( $AUX == 0 ))
then
	printf " -> Comprovant si existeix el fitxer de configuració lightdm ... "
	if [ -e "$LIGHTDM_CONF" ]
	then
		printf "OK.\n"
		printf " -> Afegint configuració de no-llistar usuaris ... "
		if [[ `cat "$LIGHTDM_CONF" | grep "greeter-hide-users=true"` = 'greeter-hide-users=true' ]]
		then
			printf "Configuració feta anteriorment.\n"
		elif [[ `cat "$LIGHTDM_CONF" | grep "greeter-hide-users=false"` = 'greeter-hide-users=false' ]]
		then
			LINIA=`cat "$LIGHTDM_CONF" | grep -n "greeter-hide-users=false" | cut -d ':' -f 1`
			sed -i ""$LINIA"s/.*/greeter-hide-users=true" "$LIGHTDM_CONF"
		else
			echo "greeter-hide-users=true" >> "$LIGHTDM_CONF"
		fi
		printf "OK.\n"
	else
		AUX=11
		printf "error.\n"
	fi
fi
#--------------------------------------------------------------------------------------------



#------- MODUL #09 --------------------------------------------------------------------------
#--- Comprovació si existeixen els arxius per crear el home amb el login.
if (( $AUX == 0 ))
then
	printf " -> Comprovant si existeixen els fitxers de configuració.\n"
	printf "    Arxiu de configuració per l'accés no gràfic amb home ... "
	if [ -e "$PAM_LOGIN" ]
	then
		printf "OK.\n"
		printf "    Arxiu de configuració per l'accés gràfic amb home ... "
		if [ -e "$PAM_LIGHTDM" ]
		then
			printf "OK.\n"
		else
			AUX=13
			printf "error.\n"
		fi
	else
		AUX=12
		printf "error.\n"
	fi
fi
#--------------------------------------------------------------------------------------------



#------- MODUL #10 --------------------------------------------------------------------------
#--- Aplicació de la configuració dels arxius per crear el home amb el login.
if (( $AUX == 0 ))
then
	printf " -> Afegint configuració per crear home amb cada login.\n"
	printf "    Accés no gràfic ... "
	if [[ `cat "$PAM_LOGIN" | grep "session required pam_mkhomedir.so skel=/etc/skel/ umask=0077"` = "session required pam_mkhomedir.so skel=/etc/skel/ umask=0077" ]]
	then
		printf "    Configuració feta anteriorment.\n"
	else
		echo "session required pam_mkhomedir.so skel=/etc/skel/ umask=0077" >> "$PAM_LOGIN"
		printf "OK.\n"
	fi
	printf "    Accés gràfic ... "
	if [[ `cat "$PAM_LIGHTDM" | grep "session required pam_mkhomedir.so skel=/etc/skel/ umask=0077"` = "session required pam_mkhomedir.so skel=/etc/skel/ umask=0077" ]]
	then
		printf "    Configuració feta anteriorment.\n"
	else
		echo "session required pam_mkhomedir.so skel=/etc/skel/ umask=0077" >> "$PAM_LIGHTDM"
		printf "OK.\n"
	fi
	printf "\n----------------------\nConfiguració feta amb éxit.\n----------------------\n"
fi
#--------------------------------------------------------------------------------------------



#------- MODUL #00 --------------------------------------------------------------------------
#--- Missatges d'error.
if (( $AUX == 1))
then
	printf "\nError[1], no s'ha executat aquest script com a administrador.\n"
elif (( $AUX == 2))
then
	printf "\nError[2], no hi ha connexió a internet.\n"
elif (( $AUX == 3))
then
	printf "\nError[3], no hi ha connexió amb el servidor web LDAP.\n"
elif (( $AUX == 4))
then
	printf "\nError[4], no s'ha pogut descarregar la plantilla de configuració LDAP.\n"
elif (( $AUX == 5 ))
then
	printf "\nError[5], no s'ha pogut instal·lar el programari necessari.\n"
elif (( $AUX == 6 ))
then
	printf "\nError[6], versió d'ubuntu no compatible amb l'script. Necessària $UBU_V\n"
elif (( $AUX == 7 ))
then
	printf "\nHas sortit del programa.\n"
elif (( $AUX == 8 ))
then
	printf "\nError[7], no hi ha connectivitat amb el servidor LDAP.\n"
elif (( $AUX == 9 ))
then
	printf "\nError[8], no s'ha pogut descarregar la plantilla de configuració PAM.\n"
elif (( $AUX == 10 ))
then
	printf "\nError[9], hi ha hagut un error en la configuració d'autenticació.\n"
elif (( $AUX == 11 ))
then
	printf "\nError[10], no existeix fitxer indispensable del lightdm ($LIGHTDM_CONF).\n"
elif (( $AUX == 12 ))
then
	printf "\nError[11], no existeix fitxer indispensable del pam ($PAM_LOGIN).\n"
elif (( $AUX == 13 ))
then
	printf "\nError[12], no existeix fitxer indispensable del pam ($PAM_LIGHTDM).\n"
fi
#--------------------------------------------------------------------------------------------
